

// import debug from 'debug';//npm Debug package
// import ResponseHandler from '../handlers/GlobalResponseHandler';
// import EmployeeSalaryRegister1 from '../models/entity/models/Salaryregister';
const Sequelize = require('sequelize');
const sequelize = new Sequelize(process.env.DB_NAME, process.env.DB_USER, process.env.DB_PASSWORD, {
  host: process.env.DB_HOST,
  dialect: process.env.DB_TYPE,
  pool: {
    max: 9,
    min: 0,
    idle: 10000
  },
});

var fs = require('fs')
var path = require('path')
let modelsFolder = '../models/entity/models/'
function requireModel(Model) {
  return require(modelsFolder + Model)(sequelize, Sequelize)
}

/**
* below code will automatically import models from the models folder
*/
const model = path.resolve(__dirname, modelsFolder);

fs.readdirSync(model).forEach(file => {
  if (file.split('.')[1] == 'ts') {
    requireModel(file)
  }
})
export default sequelize