import { Service, Action } from "moleculer-decorators";
import ResponseHandler from "../handlers/GlobalResponseHandler";
import debug from 'debug';
import { PayrollHandler } from "../handlers/PayrollHandler";
import sequelize from "./orm";
import { getMasterSalary } from '../handlers/getMasterSalaryHandler'

//migrating the tables here
sequelize.authenticate().then(() => {
    console.log("connected")
    sequelize.sync().then(r => console.log("done"))
    sequelize.on('error', (err) => {
        console.log("error")
    })
})
    .catch((err: string) => {
        console.log("error")
    });

@Service({ name: "payroll" })
class payroll {

    @Action()
    public initiate(ctx: any) {
        let payrollHandler = new PayrollHandler();
        try {
            let payrollStatus = payrollHandler.start(ctx);
            return ResponseHandler.successResponse(ctx, 200, "success from PayrollService !", { data: ctx + " payrollStatus =  " + payrollStatus })
        } catch (err) {
            return ResponseHandler.errorResponse(430, "Error from Payroll Handler ! ", err)
        }

    }

    @Action()
    public async getEmpMasterSalaryData(ctx: any) {
        if (ctx.params.empId) {
            try {
                return await getMasterSalary(ctx);
            } catch (err) {
                return ResponseHandler.errorResponse(430, "Error in fetching data from database", err)
            }
        } else {
            return ResponseHandler.errorResponse(420, "Employee Id not found in request parameters", ctx.params)
        }
    }
}

module.exports = payroll