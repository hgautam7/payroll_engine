import { configure, getLogger } from 'log4js'

const logger = getLogger();
logger.level = 'debug';
logger.debug("Some debug messages");
 
configure({
    appenders: { engine: { type: 'file', filename: 'payrollEngine.log' } },
    categories: { default: { appenders: ['engine'], level: 'error' } }
});

export default logger