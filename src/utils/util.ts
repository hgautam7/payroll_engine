/***
 * @author           : Saurabh
 * @Creation_Date    : 11th Aug 2019
 * @Description      : Helper class for util functions
 
 * @Modification_Date : 23rd Sept 2019
 * @Modified_By       : Hemant
 * @Description       : Added function to get Date from year and month
 * 
*/

import * as C from "../utils/Config"

export class Util {
    static monthArr = {0:"Apr" , 1:"May" , 2:"Jun" , 3:"Jul" , 4:"Aug" , 5 :"Sep" , 6:"Oct",7:"Nov",8:"Dec",9:"Jan",10:"Feb",11:"Mar"}
    static revMonthArr = {"Apr":0 , "May":1 ,"Jun":2 ,"Jul":3 ,"Aug":4 ,"Sep":5 , "Oct":6,"Nov":7,"Dec":8,"Jan":9,"Feb":10,"Mar":11} 
    static prosMonthArr = {"Jan":1  , "Feb":1 ,"Mar":3 ,"Apr":4 ,"May":5 ,"Jun":6 , "Jul":7,"Aug":8,"Sep":9,"Oct":10,"Nov":11,"Dec":12} 
    static dayArr = {"Sun":0 , "Mon":1 ,"Tues":2 ,"Wed":3 ,"Thrus":4 ,"Fri":5 , "Sat":6} 

    
    static getPrevMonth(month : string){
        return this.monthArr[(this.revMonthArr[month]-1) % 12]
    }

    static getRoundOff(value: number , roundOff: any){
        switch (roundOff){
            case C.RoundDown :{
                return Math.round(value-0.49)
            }
            case C.RoundUp : {
                return Math.round(value + 0.49)
            }
            case C.RoundToFive : {
                return Math.round(value/5)*5
            }
            case C.RoundToTen : {
                return Math.round(value/10)*10
            }
            default : {
                return Math.round(value)
            }  
        }    
        return Math.round(value)         

    }

    //takes month from april to mar 
    static getMonthsBetween(startMonth: string | number , endMonth: string | number){
        let startMonthInt = this.revMonthArr[startMonth]
        let endMonthInt = this.revMonthArr[endMonth]
        // if (endMonthInt - startMonthInt < 0){
        //     return []
        // }
        var monthArr: any[] = []
        for (var i = startMonthInt ; i <= endMonthInt ; ++i){
            monthArr.push(monthArr[i])
        }
        return monthArr
    }

    static getTaxableMonthRemaining(date : Date) : number{
        return 11 - this.getMonthIndex(date) 
    }

    static getMonthIndex(date:Date):number {
        return (date.getMonth()+9)%12
    }

    static getMonth(date:Date):string {
        return this.monthArr[(date.getMonth()+9)%12]
    }

    static getDaysInAMonth(year , month){
        return new Date(year , month , 0).getDate()
    }

    static getMonthDiff(from : Date , to  : Date){
         return ( 12 * (to.getFullYear() - from.getFullYear()) + to.getMonth() - from.getMonth())
    }

    static getDate(year: number, month: string){
        return new Date(year, this.prosMonthArr[month.substring(0,3)], 1);
    }    
}  



