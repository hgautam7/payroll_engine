/***
 * @author           : Saurabh
 * @Creation_Date    : 11th Aug 2019
 * @Description      : Helper class for util functions
 
 * @Modification_Date : 23rd Sept 2019
 * @Modified_By       : Hemant
 * @Description       : Added Separator
 * 
*/
//component type

export const Earning = 'Earnings'
export const Deduction = 'Deduction'
export const EmployerContribution = 'er_cont'
export const Statutory = 'statutory'
export const Total = 'Total'
export const Loan = 'loan'
export const Perk = 'perk'
export const CTC  = 'CTC'
export const SpecialAllowance = 'SA'
export const VariableTotal = 'VF'
export const TDS = 'TDS'

//component Names/Abbreviations

export const NegativeSalaryName = 'Negative Salary'
export const NegativeSalaryAbbreviation = 'NegSal'
export const Basic = 'Basic'
export const HRA = 'HRA'
export const DA = 'DA'
export const Bonus = 'Bonus'
export const Food = 'Food'
export const PF = 'EPF'
export const EmployerPF = 'ErPF'
export const VoluntaryPF = 'VPF'
export const PT = 'PT'
export const LeaveEncashment = 'LE'
export const ESICEmployee = 'ESIC'

// pf calculation type

export const VpfFixed = 'Fixed'
export const VpfPercentage = 'Per'
export const PfGovtBody = 'government'
export const PfLocalTrust = 'pf_trust'

// roundOff

export const RoundUp:string = 'RoundUp'
export const RoundDown:string = 'RoundDown'
export const RoundToFive:string = 'RoundToFive'
export const RoundToTen:string = 'RoundToTen'


// LWF

export const LwF = 'LWF'
export const LwfFrequencyAnnual = 'annual'
export const LwfFrequencyHalfYearly = 'halfYearly'
export const LwfFrequencyMonthly = 'monthly'

// Loan

export const loanInstallmentPayrollDeduction = 'DFP'
export const loanInstallmentDeductedFromPayroll = 'DtdFP'
export const loanIntallmentExternalDeduction = 'ExtD'
export const LoanPrinciple = 'LoanPr'
export const LoanInterest = 'LoanInt'
export const LoanPerk = 'LoanPerk'
export const StandardDeductionOnRent = 30

export const SeniorCitizenAge = 60

// Table static messages

export const createTableSucc = 'table created successfully'
export const createTableErr  = 'An error occur while creating table'

// Perk 

export const CarPerk = "CnD"
export const CLAPerk = "CLA"

// General String Literal constants

export const Separator = "/"

// ITSection

export const SectionTen = "Section10"
export const SectionSixA = "SectionVIA"

//Employee Salary Status
export const StopSalary = "stop_salary"