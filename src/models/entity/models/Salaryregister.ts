//define models this way
module.exports = (sequelize, DataTypes) => {
  const salaryRegister = sequelize.define('salaryRegister', {
      id: {
        type: DataTypes.UUID,
        allowNull: false,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4
      },
      employee_id: {
        type: DataTypes.STRING,
        allowNull: true
      },
      legal_entity_id: {
        type: DataTypes.STRING,
        allowNull: true
      },
      financial_year: {
        type: DataTypes.STRING,
        allowNull: true
      },
      previous_employer: {
        type: DataTypes.JSONB,
        allowNull: true
      },
      master_salary: {
        type: DataTypes.ARRAY(DataTypes.JSONB),
        allowNull: true
      },
    }, {
      tableName: 'salaryRegister' })
     return salaryRegister;
  }
  