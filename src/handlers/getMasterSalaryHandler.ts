import sequelize from "../services/orm";


export async function getMasterSalary(ctx){
    let salaryregister=sequelize.import('../models/entity/models/Salaryregister')
    try{
     const data = await salaryregister.findOne({ where: { employee_id: ctx.params.empId } });
     console.log('----------------------salary register result-------', data)
     return data
    }catch(err){
     throw new Error(err)
    }
}