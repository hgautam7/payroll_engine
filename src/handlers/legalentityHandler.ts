/***
 * @author              : Hemant
 * @Creation_Date       : 26th Sept 2019
 * @Description         : Exacted code from Payroll handler to Legal Entity Handler for better readability. 
 *                        This handler invokes multiple services from different node to 
 *                        build the Legal Entity Object as per Legal Entity Component
 * @author              : Hemant
 * @Modification_Date   : 26th Sept - 25th Oct 2019
 * @Description         : Modified data building of LE Componenet
*/

import { isUndefined } from "util";
import { SalaryComponentCalculation } from "../../src/Components/SalaryComponentCalculation";
import { SalaryStructure } from "../../src/Components/SalaryStructure";
import { ESICSettings } from "../Components/ESICSettings";
import { LegalEntity } from "../Components/LegalEntity";
import { LePFSettings } from "../Components/LePFSettings";
import { Location } from "../Components/Location";
import { PTSettings } from "../Components/PTSettings";
import { SalaryComponent } from "../Components/SalaryComponent";
import ResponseHandler from "./GlobalResponseHandler";
import { Util } from "../../src/utils/util";

//TODO: Hemant : 1 .Replace debug instead of console.log in last commit
export async function getLegalEntityObj(ctx: any, month: string, financialYear: string) {

  let legalEntityObj: any;
  let legalEntityPFObj: any;
  let legalEntityLocObj: any;
  let legalEntityStatutoryBonus: any;
  let legalEntitySalaryCompObj: any;
  let legalEntityGratuityObj: any;
  let le_locations: any = []
  let ptSettings: any = [];
  let legalEntitySSObj: any
  let lePFSettingObj: any
  let legalEntityPayrollCycle: any
  try {
    let legalEntity = await getLegalEntityData(legalEntityPFObj, ctx, lePFSettingObj, legalEntitySalaryCompObj, legalEntityObj,
                                                      legalEntityStatutoryBonus, legalEntityLocObj, le_locations, ptSettings,
                                                      legalEntityGratuityObj, legalEntityPayrollCycle, legalEntitySSObj);
      console.log("Successfully LE - LegalEntityHandler[Payroll] !", { data: this.legalEntity })
      return legalEntity
  } catch (err) {
    ResponseHandler.errorResponse(736, 'Failure while building - LegalEntityHandler[Payroll]', err)
  }
}

/**
 * @description This function gets Legal Entity related data from various services and build LE Object 
 * @param legalEntityPFObj 
 * @param ctx 
 * @param lePFSettingObj 
 * @param legalEntitySalaryCompObj 
 * @param legalEntityObj 
 * @param legalEntityStatutoryBonus 
 * @param legalEntityLocObj 
 * @param le_locations 
 * @param ptSettings 
 * @param legalEntityGratuityObj 
 * @param legalEntityPayrollCycle 
 * @param legalEntitySSObj 
 */
async function getLegalEntityData(legalEntityPFObj: any, ctx: any, lePFSettingObj: any, legalEntitySalaryCompObj: any, legalEntityObj: any,
  legalEntityStatutoryBonus: any, legalEntityLocObj: any, le_locations: any, ptSettings: any,
  legalEntityGratuityObj: any, legalEntityPayrollCycle: any, legalEntitySSObj: any) {
  var salaryComponentsID: Array<SalaryComponent> = [];
  var salaryStructs: Array<SalaryStructure> = [];
  /******** Getting Legal Entity PF details ****************** ************/
  ({ legalEntityPFObj, lePFSettingObj } = await getlegalEntityPFObj(legalEntityPFObj, ctx, lePFSettingObj));

  let legalEntityEsicObj = []; //await broker.call('v2.legalentity.getDataByID', ctx.params.body.legal_entity_id, '');

  /***************Getting Legal Entity Salary Components *****************/
  ({ salaryComponentsID, legalEntitySalaryCompObj } = await getlegalEntitySalaryCompObj(legalEntitySalaryCompObj, ctx));

  /***************Getting Legal Entity Object ***************************/
  legalEntityObj = await getlegalEntityObj(legalEntityObj, ctx);

  /**********Getting Legal Entity Statutory Bonus details ************* */
  legalEntityStatutoryBonus = await getlegalEntityStatutoryBonus(legalEntityStatutoryBonus, ctx);

  /********Getting Legal Entity LOcation details ********************* */
  legalEntityLocObj = await getLegalEntityLocObj(legalEntityLocObj, ctx, le_locations, ptSettings);

  /********Getting Legal Entity Gratuity Details ********************* */
  legalEntityGratuityObj = await getlegalEntityGratuityObj(legalEntityGratuityObj, ctx);

  /********Getting Legal Entity Payroll Computation details ********* */
  legalEntityPayrollCycle = await getlegalEntityPayrollCycle(legalEntityPayrollCycle, ctx);

  /******* Getting Legal Entity Salary Structure details************ */
  ({ salaryStructs, legalEntitySSObj } = await getLegalEntitySSObj(legalEntitySSObj, ctx, salaryStructs));

  return buildLegalEntity(ctx, legalEntityObj, salaryStructs, legalEntityPFObj, legalEntityStatutoryBonus,
    ptSettings, le_locations, salaryComponentsID, legalEntityPayrollCycle, legalEntityGratuityObj)
}
/**
 * This function gives Legal Entity PF details for a legal entity Id
 * @returns legalEntityPFObj
 * @param legalEntityPFObj 
 * @param ctx 
 * @param lePFSettingObj 
 */
async function getlegalEntityPFObj(legalEntityPFObj: any, ctx: any, lePFSettingObj: any) {
  try {
    legalEntityPFObj = await ctx.broker.call('v2.statutory_info.findPF', { le_id: ctx.params.body.legal_entity_id });
    if (legalEntityPFObj != 'undefined' && legalEntityPFObj != null && legalEntityPFObj.payload) {
      legalEntityPFObj = legalEntityPFObj.payload.data;
      lePFSettingObj = new LePFSettings(legalEntityPFObj);
    }
    else {
      legalEntityPFObj = {};
    }
    ResponseHandler.successResponse(ctx, 200, "Success from Statutory_Info service - LegalEntityHandler[Payroll] !", { data: legalEntityPFObj });
  }
  catch (err) {
    if (err.type == 'REQUEST_REJECTED') {
      console.log('Request is Rejected for statutory_info.findPF - LegalEntityHandler[Payroll]', err);
      ResponseHandler.errorResponse(741, 'Request is Rejected for statutory_info.findPF - LegalEntityHandler[Payroll]', err);
    } else if (err.name == 'RequestTimeoutError') {
      console.log('Request time out error for statutory_info.findPF - LegalEntityHandler[Payroll]', err);
      ResponseHandler.errorResponse(742, 'Request time out error for statutory_info.findPF - LegalEntityHandler[Payroll]', err);
    } else {
      console.log('Failure from Statutory_Info service - LegalEntityHandler[Payroll]', err);
      ResponseHandler.errorResponse(728, 'Failure from Statutory_Info service - LegalEntityHandler[Payroll]', err);
    }
  }
  return { legalEntityPFObj, lePFSettingObj };
}

/**
 * This function gives Legal Entity Salary Components
 * @returns salaryComponentsID, legalEntitySalaryCompObj 
 * @param legalEntitySalaryCompObj 
 * @param ctx 
 */
async function getlegalEntitySalaryCompObj(legalEntitySalaryCompObj: any, ctx: any) {
  try {
    legalEntitySalaryCompObj = await ctx.broker.call('v2.salarycomponentLE.list', { le_id: ctx.params.body.legal_entity_id });
    if (legalEntitySalaryCompObj != 'undefined' && legalEntitySalaryCompObj != null && legalEntitySalaryCompObj.payload) {
      legalEntitySalaryCompObj = legalEntitySalaryCompObj.payload.data;
      var salaryComponentsID: Array<SalaryComponent> = [];
      legalEntitySalaryCompObj.forEach((element: any) => {
        var SalaryCompCalArr: Array<SalaryComponentCalculation> = [];
        salaryComponentsID[element.code] = new SalaryComponent(element);
        salaryComponentsID[element.code].code = element.code;
        SalaryCompCalArr.push(new SalaryComponentCalculation(element.calculation[0]));
        salaryComponentsID[element.code].calculation = SalaryCompCalArr;
        salaryComponentsID[element.code].calculationType = element.calculation_type;
        salaryComponentsID[element.code].itSection = element.it_section;
        salaryComponentsID[element.code].type = element.transaction_type;
        salaryComponentsID[element.code].considerEsiEligibility = element.consider_esi_eligibility;
        salaryComponentsID[element.code].taxable = element.taxable;
        salaryComponentsID[element.code].ptApplicability = element.pt_applicability

      });
    }
    else {
      legalEntitySalaryCompObj = {};
    }
    ResponseHandler.successResponse(ctx, 200, "Success from salarycomponentLE service - LegalEntityHandler[Payroll] !", { data: legalEntitySalaryCompObj });
  }
  catch (err) {
    if (err.name == 'RequestRejectedError') {
      console.log('Request is Rejected for salarycomponentLE service - LegalEntityHandler[Payroll]', err);
      ResponseHandler.errorResponse(741, 'Request is Rejected for salarycomponentLE service - LegalEntityHandler[Payroll]', err);
    } else if (err.name == 'RequestTimeoutError') {
      console.log('Request time out error for salarycomponentLE service - LegalEntityHandler[Payroll]', err);
      ResponseHandler.errorResponse(742, 'Request time out error for salarycomponentLE service - LegalEntityHandler[Payroll]', err);
    } else {
      console.log('Failure from salarycomponentLE service - LegalEntityHandler[Payroll]', err);
      ResponseHandler.errorResponse(729, 'Failure from salarycomponentLE service - LegalEntityHandler[Payroll]', err);
    }
  }
  return { salaryComponentsID, legalEntitySalaryCompObj };
}

/**
 * @returns legalEntityObj
 * @param legalEntityObj 
 * @param ctx 
 */
async function getlegalEntityObj(legalEntityObj: any, ctx: any) {
  try {
    legalEntityObj = await ctx.broker.call('v2.legalentity.getDataByID', { id: ctx.params.body.legal_entity_id });
    if (legalEntityObj == 'undefined' && legalEntityObj == null)
      legalEntityObj = {};
    /*  else {
       legalEntityObj = legalEntityObj.payload.data
     } */
    ResponseHandler.successResponse(ctx, 200, "Success from legalentity service - LegalEntityHandler[Payroll] !", { data: legalEntityObj });
  }
  catch (err) {
    console.log('Failure from legalentity service', err);
    ResponseHandler.errorResponse(730, 'Failure from legalentity service - LegalEntityHandler[Payroll]', err);
  }
  return legalEntityObj;
}
/**
 * @returns legalEntityStatutoryBonus
 * @param legalEntityStatutoryBonus 
 * @param ctx 
 */
async function getlegalEntityStatutoryBonus(legalEntityStatutoryBonus: any, ctx: any) {
  try {
    legalEntityStatutoryBonus = await ctx.broker.call('v2.statutory_bonus.list', { le_id: ctx.params.body.legal_entity_id });
    if (legalEntityStatutoryBonus != 'undefined' && legalEntityStatutoryBonus != null && legalEntityStatutoryBonus.payload)
      legalEntityStatutoryBonus = legalEntityStatutoryBonus.payload.data;
    else {
      legalEntityStatutoryBonus = {};
    }
    ResponseHandler.successResponse(ctx, 200, "Success from statutory_bonus service - LegalEntityHandler[Payroll] !", { data: legalEntityStatutoryBonus });
  }
  catch (err) {
    console.log('Failure from statutory_bonus service', err);
    ResponseHandler.errorResponse(731, 'Failure from statutory_bonus service - LegalEntityHandler[Payroll]', err);
  }
  return legalEntityStatutoryBonus;
}

/**
 * @returns getLegalEntityLocObj
 * @param legalEntityLocObj 
 * @param ctx 
 * @param le_locations 
 * @param ptSettings 
 */
async function getLegalEntityLocObj(legalEntityLocObj: any, ctx: any, le_locations: any, ptSettings: any) {
  try {
    legalEntityLocObj = await ctx.broker.call('v2.location.list', { le_id: ctx.params.body.legal_entity_id });
    if (legalEntityLocObj != 'undefined' && legalEntityLocObj != null && legalEntityLocObj.payload) {
      legalEntityLocObj = legalEntityLocObj.payload.data;
      legalEntityLocObj.forEach((element: any) => {
        let obj: any = {};
        let objpt: any = {};
        if (element.address) {
          obj.lwfState = element.address.stateID.state_name;
          obj.address = element.address;
          obj.lwfAddress = element.lwf_address;
          obj.ptStateId = element.ptStateId;
        }
        else {
          obj.lwfState = '';
        }
        if (element.ptState) {
          obj.ptState = element.ptState.display_name;
          objpt.ptState = element.ptState.display_name;
        }
        else {
          obj.ptState = '';
          objpt.ptState = '';
        }
        obj = {
          ...obj, ...{
            id: element.id,
            lwfApplicable: element.lwf_applicable,
            wwfApplicable: element.wwf_applicable,
            deductionApplicability: element.DeductionApplicability,
            esiApplicable: element.esiApplicable,
            standardEsiContribution: element.standardESIContribution,
            address: element.address,
            ptState : element.ptState.display_name,
            ptStateId : element.ptState.id
          }
        };
        le_locations[element.id] = new Location(obj);
        le_locations[element.id].ptApplicable = element.pt_applicable
        le_locations[element.id].ptDeductMonthly = (element.pt_deduction_monthly == null) ? false : element.pt_deduction_monthly
        le_locations[element.id].ptStateId = element.ptState.id
        //***********making pt setting object for each location */
        objpt = {
          ...objpt, ...{
            stateId: element.ptStateID,
            //hard coded value--to be changed later
            deductTaxMonthly: true //not coming from UI
          }
        };
        let ptSetting = new PTSettings(objpt);
        ptSettings[element.ptStateID] = ptSetting;
      });
    }
    else {
      legalEntityLocObj = {};
    }
    ResponseHandler.successResponse(ctx, 200, "Success from location service - LegalEntityHandler[Payroll] !", { data: legalEntityLocObj });
  }
  catch (err) {
    console.log('Failure from statutory_bonus service', err);
    ResponseHandler.errorResponse(732, 'Failure from statutory_bonus service - LegalEntityHandler[Payroll]', err);
  }
  return legalEntityLocObj;
}

/**
 * @returns legalEntityGratuityObj
 * @param legalEntityGratuityObj 
 * @param ctx 
 */
async function getlegalEntityGratuityObj(legalEntityGratuityObj: any, ctx: any) {
  try {
    legalEntityGratuityObj = await ctx.broker.call('v2.statutory_info.findGRATUITY', { le_id: ctx.params.body.legal_entity_id });
    if (legalEntityGratuityObj != 'undefined' && legalEntityGratuityObj != null && legalEntityGratuityObj.payload)
      legalEntityGratuityObj = legalEntityGratuityObj.payload.data;
    else {
      legalEntityGratuityObj = {};
    }
    ResponseHandler.successResponse(ctx, 200, "Success from statutory_info[Gratuity] service - LegalEntityHandler[Payroll] !", { data: legalEntityGratuityObj });
  }
  catch (err) {
    console.log('Failure from statutory_info[Gratuity] service', err);
    ResponseHandler.errorResponse(733, 'Failure from statutory_info[Gratuity] service - LegalEntityHandler[Payroll]', err);
  }
  return legalEntityGratuityObj;
}

/**
 * @returns legalEntityPayrollCycle
 * @param legalEntityPayrollCycle 
 * @param ctx 
 */
async function getlegalEntityPayrollCycle(legalEntityPayrollCycle: any, ctx: any) {
  try {
    legalEntityPayrollCycle = await ctx.broker.call('v2.payroll_computation.list', { le_id: ctx.params.body.legal_entity_id });
    if (legalEntityPayrollCycle != 'undefined' && legalEntityPayrollCycle != null && legalEntityPayrollCycle.payload)
      legalEntityPayrollCycle = legalEntityPayrollCycle.payload.data;
    else {
      legalEntityPayrollCycle = {};
    }
    ResponseHandler.successResponse(ctx, 200, "Success from payroll_cycle service - LegalEntityHandler[Payroll] !", { data: legalEntityPayrollCycle });
  }
  catch (err) {
    console.log('Failure from payroll_cycle service', err);
    ResponseHandler.errorResponse(740, 'Failure from payroll_cycle service - LegalEntityHandler[Payroll]', err);
  }
  return legalEntityPayrollCycle;
}

/**
 * @returns  legalEntitySSObj , salaryStructs
 * @param legalEntitySSObj 
 * @param ctx 
 * @param salaryStructs 
 */
async function getLegalEntitySSObj(legalEntitySSObj: any, ctx: any, salaryStructs: any) {
  try {
    legalEntitySSObj = await ctx.broker.call('v2.salary_structure.list', { le_id: ctx.params.body.legal_entity_id });
    let salcompname: any = [];
    if (legalEntitySSObj != 'undefined' && legalEntitySSObj != null && legalEntitySSObj.payload) {
      legalEntitySSObj = legalEntitySSObj.payload;
      legalEntitySSObj.forEach((arrObj: any) => {
        arrObj.sssc.forEach((element: any) => {
          salcompname.push(element.scle.name);
          salaryStructs[arrObj.id] = {
            "id": arrObj.id,
            "name": arrObj.name,
            "salaryComponents": salcompname
          };
        });
      });
    }
    else {
      legalEntitySSObj = {};
    }
    ResponseHandler.successResponse(ctx, 200, "Success from salary_structure service - LegalEntityHandler[Payroll] !", { data: legalEntitySSObj });
  }
  catch (err) {
    console.log('Failure from salary_structure service', err);
    ResponseHandler.errorResponse(735, 'Failure from salary_structure service - LegalEntityHandler[Payroll]', err);
  }
  return { salaryStructs, legalEntitySSObj };
}

/**
 * @description This function derive basedays on the basis of paydaysvalue 
 * @param payDaysValue 
 * @param month 
 * @param financialYear 
 */
function getBaseDays(payDaysValue: any, month: string, financialYear: string) {
  let daysCount = 0
  let daysInMonth: number
  try {
    if (isUndefined(payDaysValue.baseDays) || payDaysValue.baseDays == "") {
      let weeklyoff = (payDaysValue.weeklyOff)
      weeklyoff.forEach((element: any) => {
        let date = Util.getDate(+financialYear, month)
        let dayIndex = Util.dayArr[element]
        if (date.getMonth() == Util.prosMonthArr[month.substring(0, 3)]) {
          daysInMonth = Util.getDaysInAMonth(date.getFullYear(), date.getMonth())
          for (let i = 1; i <= daysInMonth; i++) {
            let calDate = new Date(+financialYear, Util.prosMonthArr[month.substring(0, 3)], i)
            if (dayIndex == calDate.getDay()) {
              daysCount += 1;
            }
          }
        }
      });
      return daysInMonth - daysCount
    } else
      return payDaysValue.baseDays
  } catch (err) {
    ResponseHandler.errorResponse(740, 'Failed in getBaseDays() - LegalEntityHandler[Payroll]', err)
  }
}
/**
 * @description This functions builds LegalEntity Object as LegalEntity Component
 * @returns LegalEntity Object 
 * @param ctx 
 * @param legalEntityObj 
 * @param salaryStructs 
 * @param legalEntityPFObj 
 * @param legalEntityStatutoryBonus 
 * @param ptSettings 
 * @param le_locations 
 * @param salarycomponents 
 * @param salaryComponentsID 
 * @param legalEntityPayrollCycle 
 * @param legalEntityGratuityObj 
 */
function buildLegalEntity(ctx: any, legalEntityObj: any, salaryStructs: any, legalEntityPFObj: any, legalEntityStatutoryBonus: any,
  ptSettings: any, le_locations: any, salaryComponentsID: any, legalEntityPayrollCycle: any,
  legalEntityGratuityObj: any) {
  let legalentity: any = {}
  try {
    legalentity = new LegalEntity({
      id: ctx.params.body.legal_entity_id,
      name: legalEntityObj.legalName,
      salaryStructures: salaryStructs,
      pfSettings: new LePFSettings({
        isApplicable: legalEntityPFObj.isApplicable,
        pfBody: legalEntityPFObj.pfBody,
        employeeContributionRestrict: legalEntityPFObj.employeeContributionRule,
        employerContributionRestrict: legalEntityPFObj.employerContributionRule,
        epsCalculationRestrict: legalEntityPFObj.espCalculationRule,
        edliChargesApplicable: legalEntityPFObj.edliChargesApplicable,
        edliInspectionChargesPartOfCtc: legalEntityPFObj.edliInspectionChargesPartOfCtc,
        adminChargesPartOfCtc: legalEntityPFObj.adminChargesPartOfCtc
      }),
      esicSettings: new ESICSettings({
        registered: '',
        employeeContribution: legalEntityPFObj.employeeContributionRule,
        employerContribution: legalEntityPFObj.employerContributionRule,
        //hard coded value -- check with saurabh
        employeeContributionOutsideGross: "",
        allowOverridingEsic: "",
        exclude_employer_share_from_esic_calculations: "",
        restrict_esic_gross_to_statuatory_gross: "",
        include_bonus_one_time_payment_for_eligiblity: "",
        include_bonus_one_time_payment_for_contribution: ""
        //end
      }),
      staturyBonus: legalEntityStatutoryBonus,
      ptSettings: ptSettings,// array with key stateid
      locations: le_locations,
      salaryComponents: salaryComponentsID,
      salaryComponentsID: salaryComponentsID,
      baseDays: getBaseDays(legalEntityPayrollCycle.pay_days_value, ctx.params.body.month, ctx.params.body.financial_year),
      gratuitySettings: legalEntityGratuityObj
    });
  } catch (err) {
    ResponseHandler.errorResponse(741, 'Failed in buildLegalEntity() - LegalEntityHandler[Payroll]', err)
  }
  return legalentity
}



