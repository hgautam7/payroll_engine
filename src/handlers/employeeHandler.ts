/***
 * @author           : Arushi
 * @Creation_Date    : 26th Sept 2019
 * @Description      : Exacted code from Payroll handler to Employee Handler for better readability. 
 *                     This handler invokes multiple services from different node to 
 *                     build Employee Object as per Employee Component
 * @Modification_Date : 21st Oct 2019 
 * @Modified_By       : Hemant 
 * @Description       : Re-structured/Refactored Handler and exception handling 
*/
import { EmployeePF } from "../../src/Components/EmployeePF";
import { Attendance } from "../Components/Attendance";
import { Employee } from "../Components/Employee";
import { EmployeeSalary } from "../Components/EmployeeSalary";
import { LegalEntity } from "../Components/LegalEntity";
import { MasterSalary } from "../Components/MasterSalary";
import { SalaryDescription } from "../Components/SalaryDescription";
import { SalaryRegister } from "../Components/SalaryRegister";
import { Util } from "../utils/util";
import ResponseHandler from "./GlobalResponseHandler";
import { EmployeeLWF } from "../../src/Components/EmployeeLWF";
import { EmployeePT } from "../../src/Components/EmployeePT";
import { EmployeeESIC } from "../../src/Components/EmployeeESIC";
import { AddressObj } from "../Components/AddressObj";
import { ItSheet } from "../../src/Components/ItSheet";
import * as C from "../utils/Config";
import { ItSheetDescription } from "../../src/Components/ItSheetDescription";
import { LOP } from "../../src/Components/LOP";
import { TdsOverride } from "../../src/Components/TdsOverride";
import { InputComponent } from "../../src/Components/InputComponent";
import { InvestmentPlanComponent } from "../../src/Components/InvestmentPlanComponent";
import { NonCtCComponent } from "../../src/Components/NonCtCComponent";
import { SalaryComponent } from "../../src/Components/SalaryComponent";

/**
  * @Description This function gives List<Employee>[with all Emp data req for running payroll]
  *              Criteria : Legal Entity Id
  * @param ctx
  * @param legalEntity 
  */
export async function getEmpListByLeId(ctx: any, legalEntity: LegalEntity, month : string) {
  console.log('************inside getEmpListByLeId*********');
  try {
    let res = await ctx.broker.call('v1.employee.getEmployeeByLegalEntityId', { query: { legal_entity_id: ctx.params.body.legal_entity_id } });
    let employees: any = []
    if (res.payload.empData) {
      res.payload.empData.forEach((element: any) => {
        //Payroll will not process employee with Stop Salary status
       // if (new Date(element.EmployeeFnf.last_working_date).getMonth != this.month) {
          let employee = preparepayrolldata(element, ctx, legalEntity)
          employees.push(employee)
        //}
      });
      return employees
    }
    else {
      ResponseHandler.errorResponse(711, "No employees present in this Legal Entity = " + ctx.params.body.legal_entity_id, { data: res })
    }
  }
  catch (err) {
    console.log("Error while getting employees present in this Legal Entity = " + ctx.params.body.legal_entity_id, err);
    ResponseHandler.errorResponse(712, "Error while getting employees present in this Legal Entity = " + ctx.params.body.legal_entity_id, err)
  }
}

/**
 * @description This function prepares the Employee Object for payroll processing
 * @param response 
 * @param ctx 
 * @param legalEntity 
 * @returns Employee
 */
function preparepayrolldata(response: any, ctx: any, legalEntity: LegalEntity) {
  let salComp: any = []
  let itSheetEarnings: any = [] // has only paid components for payslip
  let deduction: any = []
  let gross: number = 0
  let histSalComp: any = []
  let salaryStructures: any = []
  console.log("Employee_id = ", response.EmployeeOfficial.employee_id)
  /******* Step 1 : Salary Structure & Salary component build **************************************/
  // Step 1.1 - Check if any SS is applicable for current month to the employee or not
  /*  if (response.EmployeeSalarySetting[0] != 'undefined' && response.EmployeeSalarySetting[0] != null && response.EmployeeSalarySetting[0].history_salary_structure != null) {
     response.EmployeeSalarySetting[0].history_salary_structure.forEach((element: any) => {
       //This check depicts SS is applicable for current month
       if (Util.getMonth(Util.getDate(+ctx.params.body.financial_year, ctx.params.body.month)) > element.from) {
         updateSalary(response, ctx)
       }
     })
   } */
  //Step 1.2 - Building salarycomponent from data from UI &  parsing as and array of SalaryDescription
  response.EmployeeSalarySetting[0].master_salary_structure.forEach((element: any) => {
    element.id = element.id
    let saldesc = new SalaryDescription(element, element.annual)
    //gross = gross + element.annual
    salComp.push(saldesc)
  });
  //Step 1.3 Make EmployeeSalary and initialize the same. 
  let salaryStructureId = response.EmployeeSalarySetting[0].salary_structure_id

  response.salaryStructure = new EmployeeSalary({
    "salaryComponents": salComp, "salaryStructureId": salaryStructureId, "ctc": response.EmployeeSalarySetting[0].ctc
  })
  response.salaryStructure.dateUpdated = Util.getDate(+ctx.params.body.financial_year, ctx.params.body.month)
  response.salaryStructure.dateApplicable = Util.getDate(+ctx.params.body.financial_year, ctx.params.body.month)

  if (response.EmployeeSalarySetting[0].history_salary_structure != null) {
    response.EmployeeSalarySetting[0].history_salary_structure.forEach((element: any) => {
      element.details.forEach((iterator: any) => {
        iterator.id = iterator.salary_component_id
        let saldesc = new SalaryDescription(iterator, iterator.annual)
        histSalComp.push(saldesc) // later sort in decending order
        salaryStructures.push(new EmployeeSalary({ "salaryComponents": histSalComp, "salaryStructureId": response.EmployeeSalarySetting[0].salary_structure_id}))
      })
    })
  } else {
    //If history_salary_structure is null [No Salary Revisions] then pushing current salary componenet and build structures 
    salaryStructures.push(new EmployeeSalary({ "salaryComponents": salComp, "salaryStructureId": salaryStructureId, "ctc": response.EmployeeSalarySetting[0].ctc }))
  }
  response.salaryStructures = salaryStructures

  //Step 1.4 - Building Lop data for employee 
  let lopDates: Array<LOP> = []
  let rlopDates: Array<LOP> = []
  let lop: any = {}
  let rlop: any = {}
  lop = { total: 0, dates: lopDates }
  rlop = { total: 0, dates: rlopDates }

  if(response.EmployeeLop != null){
    response.EmployeeLop.forEach((element :any) => {
        lop.id = element.id
        lop.data = element.lop_date
        lop.deductionRaio = element.deduction_ratio_lop
        lopDates.push(lop)
    });
  }

  //Step 1.5 - Building InputComponents for employee
  
  let inputComponents : Array<InputComponent> = []
  let inputComponent : any = {}

  if(response.EmployeeComponent != null){
      response.EmployeeComponent.forEach((element:any) => {
        let salComp = legalEntity.salaryComponentsID[element.code]
        inputComponent = new InputComponent(element)
        inputComponent.processingMonth = Util.getDate(+ctx.params.body.financial_year, ctx.params.body.month)
       // inputComponent.type = salComp.type
        inputComponents.push(inputComponent)
      });
  }

  //Step 1.6 - Building InvestmentPlanComponents 
  let investmentPlanComponents: Array<InvestmentPlanComponent> = []
  let investmentPlanComponent : any = {}

  if(response.EmployeeInvestmentPlanComponents != null){
    response.EmployeeInvestmentPlanComponents.forEach((element: any) => {
       investmentPlanComponent = new InvestmentPlanComponent(element)
       investmentPlanComponents.push(investmentPlanComponent)
    });
  }
  
  response.employee_id = response.EmployeeOfficial.employee_id
  response.based_location_id = response.EmployeeOfficial.based_location_id

  //Step 1.7 - Building NonCTCComponents
  let nonCTCComponents: Array<InvestmentPlanComponent> = []
  let nonCTCComponent : any = {}
  let nonCTCSalComp : any = []

  if(response.EmployeeFixedNonCTCComponent != null){
    response.EmployeeFixedNonCTCComponent.forEach((element: any) => {
      nonCTCComponent = new NonCtCComponent(new SalaryComponent({"code" : element.salary_component_code, "id" : element.salary_component_id}), 
                        element.value)
      nonCTCComponent.from = element.from      
      nonCTCComponent.assignedDate = new Date//element.assigned_date  as coming null from UI  
      nonCTCComponent.to = element.to               
      nonCTCComponents.push(nonCTCComponent)
    });
  }

  /************ Step 2 - Initializing SalaryRegister using EmployeeSalaryRegisters[data from UI] *******/
  let empAdd = new AddressObj(legalEntity.locations[response.based_location_id].address)
  console.log("pkey = ", ctx.params.body.financial_year.substring(2,4)+Util.prosMonthArr[ctx.params.body.month])
  if (response.EmployeeSalaryRegisters == null) {
    let masterSalary = new MasterSalary();
    let pt = new EmployeePT()
    pt.value = 0
    pt.wage = 0
    pt.paidTillNow = 0
    pt.unpaidPT = 0
    pt.stateCode = empAdd.state
    masterSalary.pt = pt
    let masterSalLists: any = {}
    masterSalLists[ctx.params.body.financial_year.substring(2,4)+Util.prosMonthArr[ctx.params.body.month]] = masterSalary
    response.EmployeeSalaryRegisters = new SalaryRegister({
      "id": '',
      "employee_id": response.employee_id,
      "legal_entity_id": response.legal_entity_id,
      "previousEmployer": { "income": 0, "pf": 0, "pt": 0, "tds": 0 }, // TODO: Hemant , one changes are implemented in 1.1 
      "masterSalaryList": masterSalLists
    })
  }
  /************** Step 3 - Last step to initialize Employee ****************************/
  try {
    // Step 3.1 - Initilizing Employee 
    let employee = new Employee(response, legalEntity, ctx.params.body.month);
    employee.nonCtcComponents = nonCTCComponents
    employee.inputComponents = inputComponents
    
    
    employee.location.address = empAdd
    employee.pfApplicable = response.EmployeePfEsi[0].pf_enabled;
    employee.esicApplicable = true // hard coded for demo as from UI its not coming 
    employee.salaryStructures = response.salaryStructures
    employee.tdsDeducted = 0;
    //Step 3.2 - Initialize MasterSalary
    let masterSalary = new MasterSalary();
    masterSalary.lop = lop
    masterSalary.rlop = rlop

    let salComps: Array<SalaryDescription> = []
    let dedComps: Array<SalaryDescription> = []
    masterSalary.salaryStructure = employee.salaryStructure
    //initializing earnings & deductions  .
    masterSalary.earnings = { gross: 0, description: salComps }
    masterSalary.deductions = { total: 0, description: dedComps }
    //masterSalary.components = salComp
    salComp.forEach((element: any) => {
      masterSalary.components[element.code] = element
    });
    //Step 3.2.1 - Initialize EmployeePF & assign to MasterSalary
    let empPF = new EmployeePF(response.EmployeePfEsi[0])
    masterSalary.pf = empPF

    //Step 3.2.2 - Initializing EmployeeLWF & assign to MasterSalary
    let empLWF = new EmployeeLWF()
    //setting as per employee location 
    let locationObj = legalEntity.locations[response.based_location_id]
    let lwdAddress = new AddressObj(legalEntity.locations[response.based_location_id].lwfAddress)
    empLWF.lwfApplicable = locationObj.lwfApplicable//employee.location.lwfApplicable
    empLWF.wwfApplicable = locationObj.wwfApplicable
    empLWF.state = lwdAddress.state
    masterSalary.lwf = empLWF

    //Step 3.2.3 - Initializing EmployeePT & assign to MasterSalary
    let empPT = new EmployeePT()
    empPT.stateId = empAdd.stateID
    empPT.applicable = employee.location.ptApplicable
    empPT.wage = 0 // initializing 
    empPT.value = 0
    empPT.stateCode = empAdd.state
    masterSalary.pt = empPT

    //Step 3.2.4 - Initializing EmployeeESIC & assign to MasterSalary
    let empEsic = new EmployeeESIC()
    empEsic.applicable = employee.location.esiApplicable
    //empEsic.eligibility = employee.location. --TODO: hemant to check from where to fetch
    masterSalary.esic = empEsic

    //Step 3.2.5 - Initializing EmployeeESIC & assign to MasterSalary
    let empITSheet = new ItSheet()
    let itEarningDescription: any = []//{ [key: string]: ItSheetDescription }
    let otherIncome: any = []
    salComp.forEach((element: any) => {
      itEarningDescription[element.code] = new ItSheetDescription(element)
    });
    empITSheet.itEarningDescription = itEarningDescription
    empITSheet.totalEarning = gross
    empITSheet.totalPerkValue = 0 // stubbing as not coming from UI 
    empITSheet.totalOtherIncome = 0 // have to see from where it will come 
    empITSheet.totalSection24Deduction = 0
    empITSheet.totalSectionSixADeduction = 0
    empITSheet.totalSectionTenExemption = 0
    empITSheet.tdsOveride = (response.EmployeeTDSOverrides != null) ? new TdsOverride(response.EmployeeTDSOverrides) : null
    masterSalary.itSheet = empITSheet
    masterSalary.baseDays = legalEntity.baseDays
    masterSalary.inputComponents = (employee.inputComponents == undefined) ? inputComponents : employee.inputComponents
    masterSalary.nonCtcComponents = (employee.nonCtcComponents == undefined) ? nonCTCComponents : employee.nonCtcComponents
    // Step 3.3 - Assign MasterSalary to Employee
    employee.masterSalary = masterSalary
    employee.tdsOverride = (response.EmployeeTDSOverrides != null) ? new TdsOverride(response.EmployeeTDSOverrides) : null
    return employee

  } catch (err) {
    console.log('***********errro in making employee object***************************', err)
    ResponseHandler.errorResponse(708, "Error while building Employee Component", err)
  }
}

async function updateSalary(response: any, ctx: any) {
  try {
    return await ctx.broker.call('v1.employee.updateSalary', { employee_id: response.EmployeeOfficial.employee_id })
  } catch (err) {
    console.log("Failed while upating salary - Payroll", err)
    ResponseHandler.errorResponse(722, "Failed in updateSalary() - EmployeeHandler", err)
  }
}

/**
 * @returns List<Employee> for a given Legal Entity in given paygroup/s id
 *          Criteria : Legal Entity and Paygroup Id/s
 * @param ctx 
 * @param legalEntity 
 */
export async function getEmpListByLeIdPayGrpId(ctx: any, legalEntity: LegalEntity, month : string) {
  try {
    console.log("Inside getEmpListByLeIdPayGrpId !")
    let res = await ctx.broker.call('v1.employee.getEmployeeByLegalEntityIdPg', { 
      legal_entity_id: ctx.params.body.legal_entity_id,
      paygroups: ctx.params.body.paygroups
    })
   
    let employees: any = []
    if (res.payload.empDataFinal) {
      res.payload.empDataFinal.forEach((element: any) => {
        //Payroll will not process employee with Stop Salary status
         // if (new Date(element.EmployeeFnf.last_working_date).getMonth != this.month) {
          let employee = preparepayrolldata(element, ctx, legalEntity)
          employees.push(employee)
        //}
      });
      return employees
    }
    else {
      ResponseHandler.errorResponse(739, "No employees present in this Legal Entity = " + ctx.params.body.legal_entity_id +" and Paygroup = " +ctx.params.body.paygroups, { data: res })
    }
  } catch (err) {
    console.log('err in getEmpListByLeIdPayGrpId', err);
    ResponseHandler.errorResponse(710, "Error while getting Employee for this Legal entity = " + ctx.params.body.legal_entity_id + " and Pay group/s = " + ctx.params.body.paygroups, err)
  }
}  
