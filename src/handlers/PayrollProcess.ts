/***
 * @author           : Saurabh
 * @Creation_Date    : 16th Ocr 2019
 * @Description      : Dedicating Payroll Process here handler have become reading and writing should be outside logic 
 * @References       : 
 * 
 * @Modification_Date : 21st Oct 2019 
 * @Modified_By       : Hemant 
 * @Description       : Added setters for LegalEntity & Employees and made some corrections
 * 
 * @Modification_Date : 23rd Oct - 10th Nov 2019 
 * @Modified_By       : Hemant 
 * @Description       : Modification in RunPayroll & nested functions 
 * 

// *********Import for Payroll Components*****************/
import { LegalEntity } from "../Components/LegalEntity";
import { Employee } from "../Components/Employee";
import { PfRules } from "../statuary/PfRules";
import { PtRules } from "../statuary/PtRules";
import { LWFRules } from "../statuary/LWFRules";
import { ESIRules } from "../statuary/ESIRules";
import { IncomeTax } from "../statuary/IncomeTax";
import * as C from "../utils/Config";
import { InvestmentComponentType } from "../../src/Components/InvestmentComponentType";
import { Util } from "../utils/util";
import { Installment } from "../../src/Components/Loan";
import { MasterSalary } from "../../src/Components/MasterSalary";
import { EmployeePT } from "../../src/Components/EmployeePT";
import ResponseHandler from "./GlobalResponseHandler";
import { Attendance } from "../../src/Components/Attendance";


//Todo: hemant - replace console.log to debug in last commit
export class PayrollProcess {
  // private states: { [key: string]: State } //this will be global data for state where key will be id 
  private legalEntity: LegalEntity
  private employees: Array<Employee> = []
  private processingDate: Date
  private sbiInterestRate: {
    [key: string]: number;
  };
  // private salStruct: SalaryStructure;
  //private incomeTax: IncomeTax
  private esiRules: ESIRules
  private ptRules: PtRules
  private lwfRules: LWFRules
  private pfRules: PfRules
  private investmentComponentTypes: { [key: string]: InvestmentComponentType }
  
  /******************* Setters for the process *************************************/

  public setProcessingDate(date : Date){
    this.processingDate = date
    console.log("ProcessingDate = ", this.processingDate)
  }

  public setEmployees(employees: any) {
    this.employees = employees
  }

  public setPtRules(ptRules: PtRules) {
    this.ptRules = ptRules
  }

  public setLegalEntity(legalEntity : LegalEntity){
     this.legalEntity = legalEntity
  }

  public setPfRules(pfRules : PfRules){
    this.pfRules = pfRules
  }

  public setLwfRules(lwfRules : LWFRules){
    this.lwfRules = lwfRules
  }

  public setEsiRules(esiRule : ESIRules){
    this.esiRules = esiRule
  }
  // ************************Payroll Computation starts*************************//
  public runPayroll() {
    let payrollResponse : string
    try {
     // console.log("Inside Runpayroll ************* ", this.employees)
      this.employees.forEach((employee: Employee) => {
        payrollResponse =  this.calculatePayroll(employee)
      });
      return payrollResponse
    } catch (err) {
      console.log('Error from runPayroll', err);
      ResponseHandler.errorResponse(713, "Payroll processing failed ", err)
    }
  }

  private getMonth(): string {
    return Util.getMonth(this.processingDate)
  }

  private calculatePayroll(employee: Employee) {
    try {
      //1. check if there is salary revision happened this month to determine if there is arrear
      let arrearSalaryStucture = employee.checkSalaryChangedForArrears(this.processingDate)//this.date -- current payroll date ?
     // console.log("**********arrearSalaryStucture *********** = ", arrearSalaryStucture);
      if (arrearSalaryStucture != null) {
        // process for other months this will be same as this month so from now on it should be same
        //i.e now Master Salary should be the deciding factor and not employee
      }
      let salaryStructure = employee.getSalaryStructure(this.processingDate)//this.date -- current payroll date ?
      let masterSalary = new MasterSalary()
      masterSalary.assignParents(employee, this.legalEntity)
      masterSalary.month = this.processingDate //this.date -- should be wat ? hemant

      masterSalary.salaryStructure = (salaryStructure == null) ? employee.salaryStructure : salaryStructure
      masterSalary.metro = (employee.metro == null)? true : employee.metro // metro values will come from where ?
      masterSalary.rent = (employee.rent == null) ? 0 : employee.rent // rent will come from where ?
      masterSalary.baseDays = this.legalEntity.baseDays 
      masterSalary.pf = employee.masterSalary.pf
      masterSalary.lop = employee.masterSalary.lop
      masterSalary.initLopDays()
      masterSalary.rlop = employee.masterSalary.rlop
      masterSalary.initRlopDays()
      masterSalary.components = employee.masterSalary.components
      masterSalary.earnings = employee.masterSalary.earnings
      masterSalary.deductions = employee.masterSalary.deductions
      
      this.lwfRules.frequency = "annual" //temp fix , need to understand how to set it correctly 
      masterSalary.lwf = employee.masterSalary.lwf
      masterSalary.pt = employee.masterSalary.pt
      masterSalary.esic = employee.masterSalary.esic
      masterSalary.itSheet = employee.masterSalary.itSheet
      masterSalary.processSalaryStructure()
      masterSalary.itSheet.totalEarning = masterSalary.earnings.gross
      masterSalary.processInputComponents(employee.inputComponents)
      masterSalary.processNonCtcComponents(employee.nonCtcComponents)
      masterSalary.processNegativeSal()
      //this.calculatePerquisites(employee, masterSalary)
      this.calculateDeductions(employee, masterSalary)
      //commented out by hemant
    //  arrearSalaryStucture: EmployeeSalary = employee.checkSalaryChangedForArrears(this.date)
     // console.log("**********arrearSalaryStucture *********** = ", arrearSalaryStucture);
      /* if (arrearSalaryStucture != null) {
        let numberOfMonths = Util.getMonthDiff(arrearSalaryStucture.from, this.processingDate)
        let arrearMasterSalary: { [key: string]: MasterSalary }
        for (let startDate = arrearSalaryStucture.from; startDate < this.processingDate; startDate.setMonth(startDate.getMonth() + 1)) {
          let key = employee.getKey(startDate)
          let arrMasterSalary = new MasterSalary()
          arrMasterSalary.baseDays = this.legalEntity.baseDays
          let earlierSal = employee.getMasterSalaryMonth(startDate)
          arrMasterSalary.copyForArrears(earlierSal)
          arrMasterSalary.processSalaryStructure()
          this.calculatePF(employee, arrMasterSalary)
          this.calculateLwf(employee, arrMasterSalary)
          this.calculatePT(employee, arrMasterSalary)
          this.calculateEsic(employee, arrMasterSalary)
          let arrears = arrMasterSalary.getArrears(earlierSal)
          masterSalary.addArrears(arrMasterSalary.month, arrears)
        }
      } */
      this.calculateIT(employee, masterSalary)
      masterSalary.finalizeSalary()
      return "success"
    }
    catch (err) {
      console.log('Error from calculatePayroll', err);
      ResponseHandler.errorResponse(714, "Failed in  calculatePayroll() for Employee = " + employee.employee_id, err)
    }
  }

  private calculatePerquisites(employee: Employee, masterSalary: MasterSalary) {
    try {
      console.log("employee.salaryRegister = ", employee.salaryRegister)

      let prevMonthRegister = employee.salaryRegister.getLastSalary(Util.getMonthIndex(this.processingDate))
      console.log("prevMonthRegister = ",prevMonthRegister)
      if (prevMonthRegister != null && prevMonthRegister.perquisites != null) {
        masterSalary.perquisites.total = prevMonthRegister.perquisites.total
        masterSalary.perquisites.superannuation = prevMonthRegister.perquisites.superannuation
      } else {
        masterSalary.perquisites.total = 0
        masterSalary.perquisites.superannuation
      }
    //  masterSalary.perquisites.monthTotal = 0
      if (employee.perks != null) {
        employee.perks.forEach(perk => {
          let salComp = this.legalEntity.salaryComponentsID[perk.id]
          let val = salComp.calculatePerkValue(perk.amount, perk.custom)
          if (val > 0) {
            masterSalary.perquisites.total += val
        //    masterSalary.perquisites.monthTotal += val
            if (salComp.superannuationPerk) {
              masterSalary.perquisites.superannuation += val
            }
            masterSalary.insertPerk(salComp, val)
          }
        });
      }
    } catch (err) {
      console.log("Failled in  calculatePerquisites() for Employee = " + employee.employee_id, err)
      ResponseHandler.errorResponse(715, "Failed in  calculatePerquisites() for Employee = " + employee.employee_id, err)
    }
  }

  calculateLoanPerk(employee: any, month: any): number {
    try {
      if (employee.loans == null) { return 0 }
      let loanPerk = 0
      employee.loans.forEach(loan => {
        let installment: Installment = loan.getInstallment(month);
        if (installment != null) {
          if (installment.status == C.loanInstallmentPayrollDeduction) {
            let salaryComponentInterest = this.legalEntity.salaryComponents[C.LoanInterest];
            if (this.sbiInterestRate[loan.type] == null) {
              throw new Error("Unidentified Loan Type")
            }
            let sbiRate = this.sbiInterestRate[loan.type]
            let openingBal = installment.cb + installment.amount - installment.interest
            let sbiInterest = Math.round(openingBal * sbiRate / 12 / 100)
            if (sbiInterest > installment.interest) {
              loanPerk += sbiInterest - installment.interest
            }
          }
        }
      });
      return loanPerk
    }
    catch (err) {
      console.log("Failed in calculateLoan() for Employee = " + employee.employee_id, err);
      ResponseHandler.errorResponse(716, "Failed in calculateLoan() for Employee = " + employee.employee_id, err)
    }
  }

  private calculateDeductions(employee: Employee, masterSalary: MasterSalary) {
    try {
      this.calculateLoan(employee, masterSalary)
      this.calculatePF(employee, masterSalary)
      this.calculateLwf(employee, masterSalary)
      this.calculatePT(employee, masterSalary)
      this.calculateEsic(employee, masterSalary) 
    }
    catch (err) {
      console.log("Failed in calculateDeductions() for Employee = " + employee.employee_id, err);
      ResponseHandler.errorResponse(717, "Failed in calculateDeductions() for Employee = " + employee.employee_id, err)
    }

  }
  private calculatePF(employee: Employee, masterSalary: MasterSalary) {
    try {
      masterSalary.pf.applicable = (employee.pfApplicable && this.legalEntity.pfSettings.isApplicable)
      this.pfRules.calculatePF(masterSalary, employee.age)
    }
    catch (err) {
      console.log("Failed in calculatePF() for Employee = " + employee.employee_id, err);
      ResponseHandler.errorResponse(718, "Failed in calculatePF() for Employee = " + employee.employee_id, err)
    }
  }


  private calculatePT(employee: Employee, masterSalary: MasterSalary) {
    try {
      //get this financial year PT in sorted order key will be month like April 0 , May 1 ..      
      let ptList: { [key: number]: EmployeePT } = employee.salaryRegister.getPtList()
      //check if there is unpaid Pt Left last month and this month pt location is not same as last month pt location      
      let prevMonthSal = employee.getPrevMonthMasterSalary(this.processingDate)
      if (prevMonthSal != null) {
        masterSalary.pt.paidTillNow = prevMonthSal.pt.paidTillNow
        this.ptRules.checkLastMonth(masterSalary.pt, prevMonthSal.pt)
      }
      if (!employee.location.ptApplicable) {
        masterSalary.pt.applicable = false
        masterSalary.pt.unpaidPT = 0
        masterSalary.pt.value = 0
        return
      }
      this.ptRules.calculate(masterSalary.pt, ptList, employee.sex, true);
    }
    catch (err) {
      console.log("Failed in calculatePT() for Employee = " + employee.employee_id, err);
      ResponseHandler.errorResponse(719, "Failed in calculatePT() for Employee = " + employee.employee_id, err)
    }
  }

  private calculateEsic(employee: Employee, masterSalary: MasterSalary) {
    try {
      if (!employee.location.esiApplicable && !employee.esicApplicable) {
        masterSalary.esic.applicable = false
        masterSalary.esic.checkNext = true
        masterSalary.esic.employeeContribution = 0
        masterSalary.esic.employerContribution = 0
        return
      }
      let prevMonthEsic = null
      let prevMonthSal = employee.getPrevMonthMasterSalary(this.processingDate)
      if (prevMonthSal != null) {
        prevMonthEsic = prevMonthSal.esic
      }
      //console.log("city = ",employee.location.address)
      this.esiRules.calculate(masterSalary.esic, this.processingDate, prevMonthEsic, employee.location.address.city);
    }
    catch (err) {
      console.log('Error from calculateEsic', err);
      ResponseHandler.errorResponse(720, "Failed in calculateEsic() for Employee = " + employee.employee_id, err)
    }
  }

  private calculateLoan(employee: Employee, masterSalary: MasterSalary) {
    try {
      let month = Util.getMonth(this.processingDate)
      if (employee.loans == null) { return }
      let loanPerk = 0
      employee.loans.forEach(loan => {
        let installment = loan.getInstallment(month);
        if (installment != null) {
          if (this.sbiInterestRate[loan.type] == null) {
            ResponseHandler.errorResponse(721, "Unidentified Loan Type = " + this.sbiInterestRate[loan.type], new Error)
          }
          let sbiRate = this.sbiInterestRate[loan.type]
          let openingBal = installment.cb + installment.amount - installment.interest
          let sbiInterest = Math.round(openingBal * sbiRate / 12 / 100)
          if (sbiInterest > installment.interest) {
            loanPerk += sbiInterest - installment.interest
          }
          if (installment.status == C.loanInstallmentPayrollDeduction) {
            let salaryComponent = this.legalEntity.salaryComponents[C.LoanPrinciple];

            if (salaryComponent != null) {
              masterSalary.addDeduction(salaryComponent, installment.amount - installment.interest)
            }
            else {
              ResponseHandler.errorResponse(722, "Loan Component is not present = " + salaryComponent.name, new Error)
            }
            let salaryComponentInterest = this.legalEntity.salaryComponents[C.LoanInterest];
            if (salaryComponentInterest != null) {
              masterSalary.addDeduction(salaryComponentInterest, installment.interest)
            }
            else {
              ResponseHandler.errorResponse(723, "Interest Component is not present = " + salaryComponentInterest.name, new Error)
            }
            installment.status = C.loanInstallmentDeductedFromPayroll; //save it in DB
          }
        }
      });
      if (loanPerk > 0) {
        masterSalary.perquisites.total += loanPerk
        //masterSalary.perquisites.monthTotal += loanPerk
        let salComp = this.legalEntity.salaryComponents[C.LoanPerk]
        if (salComp != null) {
          if (salComp.superannuationPerk) {
            masterSalary.perquisites.superannuation += loanPerk
          }
          masterSalary.insertPerk(salComp, loanPerk)
        }
        else {
          ResponseHandler.errorResponse(724, "Loan Perk does not exist ", new Error)
          let obj = { id: "NA", name: C.LoanInterest, abbreviation: C.LoanInterest }
          masterSalary.insertPerk(obj, loanPerk)
        }
      }
    }
    catch (err) {
      console.log('Error from calculateLoan', err);
      ResponseHandler.errorResponse(725, "Failed in  calculateLoan for Employee = " + employee.employee_id, err)
    }
  }
  private calculateIT(employee: Employee, masterSalary: MasterSalary) {
    try {

      let it = new IncomeTax(this.legalEntity, employee,new Date, null, this.sbiInterestRate, this.pfRules);
      it.calculateTax(masterSalary)
    }
    catch (err) {
      console.log('Error from calculateIT', err);
      ResponseHandler.errorResponse(726, "Failed in  calculateIT for Employee = " + employee.employee_id, err)
    }
  }
 
  private calculateLwf(employee: Employee, masterSalary: MasterSalary) {
    try {
      masterSalary.lwf.wage = masterSalary.getGross()
      this.lwfRules.calculateLWF(masterSalary.lwf)
    }
    catch (err) {
      console.log('Error from calculateLwf', err);
      ResponseHandler.errorResponse(727, "Failed in  calculateLwf for Employee = " + employee.employee_id, err)
    }
  }
}
