/***
 * @author           : Hemant
 * @Creation_Date    : 11th Sept 2019
 * @Description      : This handler delegates and process the incomming request from Payroll
 *                     Service in nutshell. 
 * @References       : Payroll Handler ---> Models
 *                                  ---> Rules [Statuary]
 *                                  ---> Payroll Calculation
 * @Modification_Date : 24/25/26 Sept 
 * @Modified_By       : Arushi 
 * @Description       : Modified/Refactored the input[Object structure] for Payroll Engine
 * 
 * @Modification_Date : 21st Oct 2019 
 * @Modified_By       : Hemant 
 * @Description       : Re-structured Handler and moved all payroll to PayrollProcess 
*/

// *********Import for Payroll Components*****************//
import { LegalEntity } from "../Components/LegalEntity";
import { Employee } from "../Components/Employee";
import { PtRules } from "../statuary/PtRules";
import { ESIRules } from "../statuary/ESIRules";
//import { IncomeTax } from "../statuary/IncomeTax";
import { getLegalEntityObj } from './legalentityHandler'
import { getEmpListByLeId, getEmpListByLeIdPayGrpId } from './employeeHandler'
import { prepareMasterSalary } from './MasterSalaryHandler'
import sequelize from "../services/orm";
import { InvestmentComponentType } from "../../src/Components/InvestmentComponentType";
import { PayrollProcess } from "../../src/handlers/PayrollProcess"
import ResponseHandler from "./GlobalResponseHandler";
import { EmployeeSalary } from "../../src/Components/EmployeeSalary";
import { PfRules } from "../../src/statuary/PfRules";
import { LWFRules } from "../../src/statuary/LWFRules";
import { Util } from "../../src/utils/util";
import logger from '../utils/logger'

//Todo: hemant - replace logger.error to debug in last commit
export class PayrollHandler {
  private month: string;
  private financialYear: string;
  private processingDate: Date
  investmentComponentTypes: { [key: string]: InvestmentComponentType }
  private payrollProcess = new PayrollProcess()
  private payrollStatus: string

  //Legal Entity and Employee are read for given Month and Year in which Payroll is ran 
  start(ctx: any): any {
    this.month = ctx.params.body.month
    this.financialYear = ctx.params.body.financial_year
    this.processingDate = Util.getDate(+ctx.params.body.financial_year, ctx.params.body.month);
    logger.info('Reading Legal Entity for this month = ', this.month, ' and financial year = ', this.financialYear)
    logger.info('le  = ', ctx.params.body.legal_entity_id , ' and paygroup = ', ctx.params.body.paygroups)
    //Case 1 : Legal Entity & Paygroups
     if (ctx.params.body.legal_entity_id != null && Object.keys(ctx.params.body.paygroups[0]).length  > 0) {
      getLegalEntityObj(ctx,this.month ,this.financialYear)
        .then((legalEntityRes) => {
          getEmpListByLeIdPayGrpId(ctx, legalEntityRes, this.month)
            .then((emplist) => {
              this.setData(emplist, legalEntityRes);
              this.payrollStatus = this.payrollProcess.runPayroll();
              this.insertOrUpdateSalReg(emplist, ctx)
              return this.payrollStatus
            }).catch((err) => {
              logger.error('Failed to get Employee List', err)
              ResponseHandler.errorResponse(701, "Failed to get Employee List", err)
            });
        }).catch((err) => {
          logger.error('Failed to get Legal Entity', err)
          ResponseHandler.errorResponse(702, "Failed to get Legal Entity", err)
        })
    } //Case 2 : Legal Entity but no Paygroups
    else if(ctx.params.body.legal_entity_id != null /* && Object.keys(ctx.params.body.paygroups[0]).length == 0 */){
      getLegalEntityObj(ctx,this.month ,this.financialYear)
        .then((legalEntityRes) => {
          getEmpListByLeId(ctx, legalEntityRes, this.month)
            .then((emplist) => {
              this.setData(emplist, legalEntityRes);
              this.payrollStatus = this.payrollProcess.runPayroll();
              this.insertOrUpdateSalReg(emplist, ctx)
              return this.payrollStatus
            }).catch((err) => {
              logger.error('Failed to get Employee List', err)
              ResponseHandler.errorResponse(701, "Failed to get Employee List", err)
            });
        }).catch((err) => {
          logger.error('Failed to get Legal Entity', err)
          ResponseHandler.errorResponse(702, "Failed to get Legal Entity", err)
        })
    }
  }

  /**
   * @description This function set ProcessingDate ,Employees,LegalEntity & all Rules to Payroll process
   * @author Hemant
   * @param emplist 
   * @param legalEntityRes 
   */
  private setData(emplist: any, legalEntityRes: any) {
    this.payrollProcess.setProcessingDate(this.processingDate);
    this.payrollProcess.setEmployees(emplist);
    this.payrollProcess.setLegalEntity(legalEntityRes);
    this.payrollProcess.setPtRules(new PtRules(legalEntityRes, this.processingDate)); //ctx.date -- payroll date
    this.payrollProcess.setPfRules(new PfRules(legalEntityRes));
    this.payrollProcess.setLwfRules(new LWFRules(legalEntityRes, this.month));
    this.payrollProcess.setEsiRules(new ESIRules());
  }

  // ************************Adding payroll Result to Database*************************//

  private async insertOrUpdateSalReg(employees: Employee[], ctx: any) {
    try {
      logger.error('inside insertOrUpdateSalReg , ctx = ', ctx.params.body.legal_entity_id)
      let salaryregister = sequelize.import('../models/entity/models/Salaryregister')
      let id = ctx.params.body.legal_entity_id
      employees.forEach(async (employee: Employee) => {
        employee.masterSalary.netSalary = ((employee.masterSalary.earnings.gross) - (employee.masterSalary.deductions.total))
        let masterSalary = prepareMasterSalary(employee, this.month)
        return await salaryregister.findOne({
          where: {
            employee_id: employee.employee_id,
            legal_entity_id: id
          }
        })
          .then(async (found: any) => {
            if (found) { //Record already exit , so update master salary
              try {
                let mastersal = []
                mastersal.push(masterSalary)
                found.master_salary = mastersal;
                logger.error('*****************Record already exit , so update master salary: for employee = **************', employee.employee_id)
                logger.error("earnings := ",mastersal)
                //logger.error("earnings := ",employee.masterSalary.earnings)
                //logger.error("deductions := ",employee.masterSalary.deductions)
                //logger.error("lop := ",employee.masterSalary.lop)
                //logger.error("rlop := ",employee.masterSalary.rlop)
                // await found.save()

                return await salaryregister.update(
                  { master_salary: found.master_salary },
                  {
                    where: {
                      employee_id: employee.salaryRegister.employee_id,
                      legal_entity_id: employee.salaryRegister.legal_entity_id
                    }
                  })
              } catch (err) {
                logger.error('Failed while updating Employee Salary Register', err);
                ResponseHandler.errorResponse(705, "Failed while updating Employee Salary Register !", err)
              }
            } else { //Insert data in table
              let masterArray = []
              masterArray.push(masterSalary)
              logger.error('*****************Insert data in table:  for employee = **************', masterArray)
              logger.error("earnings := ",employee.masterSalary.earnings)
              logger.error("deductions := ",employee.masterSalary.deductions)
              logger.error("lop := ",employee.masterSalary.lop)
              logger.error("rlop := ",employee.masterSalary.rlop)
              
              try {
                salaryregister.create({
                  employee_id: employee.employee_id,
                  legal_entity_id: employee.salaryRegister.legal_entity_id,
                  financial_year: this.financialYear,
                  previous_employer: {}, //To be handled in 1.1 requirements
                  master_salary: masterArray
                })
              } catch (err) {
                logger.error('Error from insert into SalaryREgister', err);
                ResponseHandler.errorResponse(706, "Failed while inserting into Employee Salary Register !", err)
              }
            }
          });
      });
    }
    catch (err) {
      logger.error('Failed in insertOrUpdateSalReg', err);
      ResponseHandler.errorResponse(707, "Failed in insertOrUpdateSalReg !", err)
    }
  }


}
