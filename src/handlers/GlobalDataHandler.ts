import { State } from "../../src/Components/State";

export class GlobalDataHandler {
    public static StateList : {[key: string] : State} // key is stateId
    public static CodeStateList : {[key:string] : State} // key is StateCode   
}