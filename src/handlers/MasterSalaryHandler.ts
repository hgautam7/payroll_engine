const stringify = require('json-stringify-safe')
export function prepareMasterSalary(employee : any, month : any) {
    let emp = {
        "month": month,
        "pf": {
            "wage": employee.masterSalary.pf.wage,
            "employee": employee.masterSalary.pf.employeeContrib,
            "employer": employee.masterSalary.pf.employerContrib,
            "eps_wage": employee.masterSalary.pf.epsWage,
            "edli_contribution": employee.masterSalary.pf.edliContribution,
            "administration_charge": employee.masterSalary.pf.administrationCharge,
            "pf_inspection_charges": employee.masterSalary.pf.pfInspectionCharges,
            "edli_inspection_charges": employee.masterSalary.pf.edliInspectionCharges
        },
        "pt": {
            "wage": employee.masterSalary.pt.wage,
            "state": employee.masterSalary.pt.state,
            "value": employee.masterSalary.pt.value,
            "paid_till_now": employee.masterSalary.pt.paidTillNow
        },
        "lwf": employee.masterSalary.lwf,
        "vpf": employee.masterSalary.vpf,
        "esic": {
            "wage": employee.masterSalary.esic.wage,
            "employee": employee.masterSalary.esic.employeeContribution,
            "employer": employee.masterSalary.esic.employerContribution,
            "check_next": employee.masterSalary.esic.checkNext,
            "eligibility": employee.masterSalary.esic.eligibility
        },
         "lop": {
            "total": employee.masterSalary.lop.total,
            "dates": stringify(employee.masterSalary.lop.dates)
        },
        "rlop": {
            "total": employee.masterSalary.rlop.total,
            "dates": stringify(employee.masterSalary.rlop.dates)
        }, 
        "incomeTax" : employee.masterSalary.itSheet.incomeTax,
        "metro": employee.metro,
        "base_day": employee.masterSalary.baseDays,
        "earnings": employee.masterSalary.earnings,
        "it_sheet": employee.masterSalary.itSheet,
        "deductions": employee.masterSalary.deductions,
        "perquisites": [{}],
        "salary_structure": employee.masterSalary.salaryStructure
        
    }

    return emp
}

