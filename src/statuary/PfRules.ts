import { LegalEntity } from "../Components/LegalEntity";
import { Employee } from "../Components/Employee";
import * as C from "../utils/Config"
import { SalaryDescription } from "../Components/SalaryDescription";
import * as S from "./StatutoryConfig"
import { EmployeePF } from "../../src/Components/EmployeePF";
import { MasterSalary } from "../../src/Components/MasterSalary";

//added inspection charges to deduction when part of CTC 25th-09

export class PfRules {

    legalEntity: LegalEntity
    // employee : Employee
    // employerPfWage: number = 0
    // wage: number = 0
    // employeeContrib: number = 0
    // employerContrib: number = 0
    // epsWage: number = 0
    // eps: number = 0
    // edliContribution: number = 0
    // edliInspectionCharges: number = 0
    // administrationCharge: number = 0
    // pfInspectionCharges: number = 0
    // vpf: number = 0
    


    constructor(legalEntity: LegalEntity) {
        this.legalEntity = legalEntity
    }


    calcuateEPS(masterSalary : MasterSalary , age , employerPfWage) {
        if (age > S.EPSEmployeeAge) {
            masterSalary.pf.epsWage = 0
            masterSalary.pf.eps = 0
            return
        }
        masterSalary.pf.epsWage = Math.min(employerPfWage, S.maxEPSWage)
        masterSalary.pf.eps = S.EPSPercentage * masterSalary.pf.epsWage / 100
        masterSalary.pf.employerContrib = masterSalary.pf.employerContrib - masterSalary.pf.eps
    }

    calculatePF(masterSalary : MasterSalary , age) {
        let employerPfWage = 0
        if (!masterSalary.pf.applicable){
            masterSalary.pf.administrationCharge = 0
            masterSalary.pf.edliContribution = 0
            masterSalary.pf.edliInspectionCharges = 0
            masterSalary.pf.employeeContrib = 0
            masterSalary.pf.employerContrib = 0
            masterSalary.pf.eps = 0
            masterSalary.pf.epsWage = 0
            masterSalary.pf.pfInspectionCharges = 0
            masterSalary.pf.wage = 0                         
            return;
        }
        let basic = masterSalary.getComponentValue(C.Basic)
        let gross = masterSalary.getGross() 
        let hra = masterSalary.getComponentValue(C.HRA)
        let bonus = masterSalary.getComponentValue(C.Bonus)
        let food = masterSalary.getComponentValue(C.Food)
        let da = masterSalary.getComponentValue(C.DA)   
        let restrictedPfWage = this.getRestrictedPfWage(basic , gross , hra , bonus , food)
        let pfWage = this.getPfWage(basic , gross , hra , bonus , food , da)
        if (this.legalEntity.pfSettings.employeeContributionRestrict) {
            masterSalary.pf.wage = restrictedPfWage
            employerPfWage = restrictedPfWage
        }
        else {
            masterSalary.pf.wage = pfWage
            if (this.legalEntity.pfSettings.employerContributionRestrict) {
                employerPfWage = restrictedPfWage
            } else { employerPfWage = pfWage }
        }
        masterSalary.pf.employeeContrib = S.EmployeePfPercentage * masterSalary.pf.wage / 100 
        masterSalary.pf.employerContrib = S.EmployerPFPercentage * employerPfWage / 100
        this.calculateVPF(masterSalary)
        this.calcuateEPS(masterSalary , age , employerPfWage)
        this.calculateCharges(masterSalary)
        //this.checkPartOfCtc()
    }

    checkPartOfCtc(){

    }

    calculateVPF(masterSalary : MasterSalary) {        
        if (masterSalary.salaryStructure.vpf == null) { masterSalary.vpf = 0}
        if (masterSalary.salaryStructure.vpf.type == C.VpfFixed) {
            masterSalary.vpf = Math.min(masterSalary.salaryStructure.vpf.value , masterSalary.getComponentValue(C.Basic) + masterSalary.getComponentValue(C.DA))
        }
        else if (masterSalary.salaryStructure.vpf.type == C.VpfPercentage) {
            masterSalary.vpf = masterSalary.salaryStructure.vpf.value * masterSalary.pf.wage / 100
            masterSalary.vpf = Math.min(masterSalary.vpf , masterSalary.getComponentValue(C.Basic) + masterSalary.getComponentValue(C.DA))            
        }
        else {
            masterSalary.vpf = 0
        }
    }

    getRestrictedPfWage(basic , gross , hra , bonus , food): number {
        let pfWage = 0 
        if (basic < S.RestrictedPfWage) {
            pfWage = gross - hra  - bonus - food
            pfWage = Math.min(pfWage, S.RestrictedPfWage)
        }
        else {
            pfWage = S.RestrictedPfWage
        }
        if (pfWage < 0) {pfWage = 0}
        return pfWage
    }

    getPfWage(basic , gross , hra , bonus , food , da): number {
        let pfWage = 0 
        if (basic < S.RestrictedPfWage) {
            pfWage = gross - hra  - bonus - food
            pfWage = Math.min(pfWage, S.RestrictedPfWage)
        }
        else {
            pfWage = basic + da
        }
        if (pfWage < 0) {pfWage = 0}
        return pfWage

    }

    calculateCharges(masterSalary) {
        if (this.legalEntity.pfSettings.pfBody == C.PfGovtBody) {
            masterSalary.pf.administrationCharge = S.PFAdministrationChargeGovtPercentage * masterSalary.pf.wage /100
        }
        else {
            masterSalary.pf.administrationCharge = S.PFAdministrationChargeSelfPercentage * masterSalary.pf.wage /100
            masterSalary.pf.pfInspectionCharges = S.PFInspectionChargesSelfPercentage * masterSalary.pf.wage / 100
        }
        if (this.legalEntity.pfSettings.edliChargesApplicable) {
            masterSalary.pf.edliContribution = S.EdliContributionPercentage * masterSalary.pf.epsWage / 100
        }
        else { masterSalary.pf.edliInspectionCharges = masterSalary.pf.epsWage * S.EdliInspectionChargesPercentage / 100 }
        // if (this.legalEntity.pfSettings.adminChargesPartOfCtc && this.administrationCharge > 0){
        //     let obj = { id: "NA", name: S.PFAdministartionName, abbreviation: S.PFAdministartionAbbreviation }
        //     let salDesc = new SalaryDescription(obj, this.administrationCharge)
        //     this.employee.salary.insertComponent(salDesc, C.Deduction)//add admin charges in deduction
        // }
        // if (this.legalEntity.pfSettings.edliInspectionChargesPartOfCtc && this.pfInspectionCharges > 0){
        //     let obj = { id: "NA", name: S.PFInspectionChargeName, abbreviation: S.PFInspectionChargeAbbreviation }
        //     let salDesc = new SalaryDescription(obj, this.pfInspectionCharges)
        //     this.employee.salary.insertComponent(salDesc, C.Deduction)//add admin charges in deduction
        //}
    }

    getMonthlyPf(basic:number , gross:number , bonus:number , hra :number , food : number , da:number ){
        let pfWage = 0
        if (this.legalEntity.pfSettings.employeeContributionRestrict) {
            pfWage = this.getRestrictedPfWage(basic ,gross , hra , bonus , food)
        } else {
            pfWage = this.getPfWage(basic,gross,hra,bonus,food,da)
        }
        return Math.round(pfWage * S.EmployeePfPercentage / 100)
    }

    getMonthlyVpf(vpf : {type:string , value : number},  basic:number , gross:number , bonus:number , hra :number , food : number , da:number ){
        let pfWage = 0
        if (this.legalEntity.pfSettings.employeeContributionRestrict) {
            pfWage = this.getRestrictedPfWage(basic ,gross , hra , bonus , food)
        } else {
            pfWage = this.getPfWage(basic,gross,hra,bonus,food,da)
        }
        let vpfValue = 0
        if (vpf == null) { vpfValue = 0}
        else if (vpf.type == C.VpfFixed) {
            vpfValue = Math.min(vpfValue ,basic + da)
        }
        else if (vpf.type == C.VpfPercentage) {
            vpfValue = vpf.value * pfWage / 100
            vpfValue = Math.min(vpfValue ,basic + da)            
        }
        return Math.round(vpfValue)
    }
    getPfValue(){

       // return {wage:this.wage , employeeContrib:this.employeeContrib , employerContrib:this.employerContrib , epsWage:this.epsWage ,eps:this.eps, edliContribution:this.edliContribution
        //    , edliInspectionCharges:this.edliInspectionCharges , administrationCharge:this.administrationCharge , pfInspectionCharges:this.pfInspectionCharges }
    }

}


