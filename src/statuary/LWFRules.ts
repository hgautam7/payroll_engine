import { LegalEntity } from "../Components/LegalEntity";
import { Employee } from "../Components/Employee";
import { Location } from "../Components/Location";
import { LWFRuleList } from "./LWFRuleList";
import * as C from "../utils/Config"
import { EmployeeLWF } from "../../src/Components/EmployeeLWF";
import ResponseHandler from "../../src/handlers/GlobalResponseHandler";

export class LWFRules {


    legalEntity: LegalEntity
    //state: string
    //wage: number = 0
    //employeeContrib = 0
    //employerContrib = 0
    lwfRule: any
    month: string
    //@added by hemant
    frequency: string  

    constructor(legalEntity: LegalEntity, month : any) {
        this.legalEntity = legalEntity
        this.month = month
        //this.frequency = legalEntity.locations[location_id]
    }

    calculateLWF(currentLwf : EmployeeLWF) {
        if (!(currentLwf.wwfApplicable || currentLwf.lwfApplicable)) {
            currentLwf.employeeContribution = 0
            currentLwf.employerContribution = 0
            currentLwf.total = 0
            return
        }
        this.lwfRule = LWFRuleList[currentLwf.state]
        if(this.lwfRule == undefined){
            console.log("State = ",currentLwf.state," is not maintained in LWFRuleList") 
            ResponseHandler.errorResponse(728, "Failed while calculateLWF !", new Error("State = "+currentLwf.state+ " is not maintained in LWFRuleList"))
        }else {
        if (this.lwfRule.frequency == C.LwfFrequencyAnnual) {
            this.processAnnual(currentLwf)
        }
        else if (this.lwfRule.frequency == C.LwfFrequencyHalfYearly) {
            this.processHalfYearly(currentLwf)
        }
        else if ((this.lwfRule.frequency == C.LwfFrequencyMonthly)) {
            this.processMonthly(currentLwf)
        }
        else { 
            console.log("LWF Error") 
            ResponseHandler.errorResponse(728, "Failed while calculateLWF !", new Error("LWF Error"))
        }
      }
    }


    processAnnual(currentLwf) {
        if (this.month == this.lwfRule.month) {
            this.getSalary(currentLwf)
        }
    }

    processHalfYearly(currentLwf) {
        if (this.lwfRule.month.includes(this.month))
            this.getSalary(currentLwf)
    }

    processMonthly(currentLwf) {
        this.getSalary(currentLwf)
    }

    getSalary(currentLwf : EmployeeLWF) {
        if (this.lwfRule.bands == null) {
            if (this.lwfRule.wage == 'all' || currentLwf.wage <= this.lwfRule.wage) {
                currentLwf.employeeContribution = this.lwfRule.employee
                currentLwf.employerContribution = this.lwfRule.employer
            }
        }
        else {
            let bands = this.lwfRule.bands
            for (let i = 0; i < bands.length; ++i) {
                let calculationTried = false
                let match = true
                if (bands[i].ge) {
                    match = (bands[i].ge <= currentLwf.wage)
                    calculationTried = true
                }
                if (bands[i].le) {
                    match = (bands[i].le >= currentLwf.wage)
                    calculationTried = true
                }
                if (match && calculationTried) {
                    currentLwf.employeeContribution = this.lwfRule.bands[i].employee
                    currentLwf.employerContribution = this.lwfRule.bands[i].employer
                    return
                }
            }
            throw new Error("LWF")
        }
    }

    getLWF() {
       // return { wage: this.wage, employer: this.employerContrib, employee: this.employeeContrib, total: (this.employeeContrib + this.employerContrib), state: this.state }
    }

}