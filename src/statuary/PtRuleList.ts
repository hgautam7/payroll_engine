export const PtRuleList = {

        "Andhra Pradesh" :{
        "State" : "Andhra Pradesh",
        "type"  : "gross",
         "frequency" : "monthly",
         "bands" : [{
             "ge" : 0,
             "le" : 15000,
             "pt" : 0
         },{
            "ge" : 15001,
            "le" : 20000,
            "pt" : 150
        },{
            "ge" : 20001,
            "pt" : 200
        }
        ]
        },
        "Andaman & Nicobar Islands":{
            "State" : "Andaman & Nicobar Islands",
            "type"  : "gross",
             "frequency" : "monthly",
             "bands" : [{
                 "ge" : 0,
                 "le" : 15000,
                 "pt" : 0
             },{
                "ge" : 15001,
                "le" : 20000,
                "pt" : 150
            },{
                "ge" : 20001,
                "pt" : 200
            }
            ]
        },
        "Assam" :{
            "State" : "Assam",
            "type"  : "gross",
             "frequency" : "monthly",
             "bands" : [{
                 "ge" : 0,
                 "le" : 10000,
                 "pt" : 0
             },{
                "ge" : 10001,
                "le" : 15000,
                "pt" : 150
            },{
                "ge" : 15001,
                "le" : 25000,
                "pt" : 180
            },
            {
                "ge" : 25001,
                "pt" : 208
            }
            ]
            },

            "Bihar":{
                "State": "Bihar",
                "type"  : "gross",
             "frequency" : "monthly",
             "bands" : [{
                 "ge" : 0,
                 "le" : 25000,
                 "pt" : 0
             },{
                "ge" : 25001,
                "le" : 41666,
                "pt" : 83.33
            },{
                "ge" : 41667,
                "le" : 83333,
                "pt" : 167.67
            },
            {
                "ge" : 83334,
                "pt" : 208.33
            }
            ]

            },
            
        "Gujarat": {
            "State": "Gujarat",
            "type": "gross",
            "frequency": "monthly",
            "bands": [{
                "ge": 0,
                "le": 5999,
                "pt": 0
            }, {
                "ge": 6000,
                "le" : 8999,
                "pt": 80
            }, {
                "ge": 9000,
                "le" : 11999,
                "pt": 150
            },{
                "le":12000,
                "pt" : 200
            }
        ]
        },
        "Kerela":{
            "State" : "Kerela",
            "type" : "gross",
            "frequency" : "yearly",
            "bands": [{
                "ge": 0,
                "le": 11999,
                "pt": 0
            }, {
                "ge": 12000,
                "le" : 17999,
                "pt": 120
            },{
                "ge": 18000,
                "le" : 29999,
                "pt": 180
            },{
                "ge": 30000,
                "le" : 44999,
                "pt": 300
            },{
                "ge": 45000,
                "le" : 59999,
                "pt": 450
            },{
                "ge": 60000,
                "le" : 74999,
                "pt": 600
            },{
                "ge": 75000,
                "le" : 99999,
                "pt": 750
            },{
                "ge": 100000,
                "le" : 124999,
                "pt": 1000
            },{
                "ge": 125000,
              
                "pt": 1250
            }
        ]

        },
        
        "Manipur":{
            "State" : "Manipur",
            "type" : "gross",
            "frequency" : "0",
            "bands": [{
                "ge": 0,
                "le": 4167,
                "pt": 0
            }, {
                "ge": 4167,
                "le" : 6250,
                "pt": 100
            },{
                "ge": 6251,
                "le" : 8333,
                "pt": 167
            },{
                "ge": 8334,
                "le" : 10416,
                "pt": 200
            },{
                "ge": 10417,
              
                "pt": 208
            }
        ]

        },

        "Meghalay": {
            "State": "Meghalay",
            "type": "gross",
            "frequency": 1,
            "bands": [{
                "ge": 0,
                "le": 4166,
                "pt": 0
            }, {
                "ge": 4167,
                "le": 6250,
                "pt": 16.50
            }, {
                "ge": 6251,
                "le": 8333,
                "pt": 25
            }, {
                "ge": 8334,
                "le": 12500,
                "pt": 41.50
            },{
                "ge" : 12501,
                "le" : 16666,
                "pt" : 62.50
            },{
                "ge" : 16667,
                "le" : 20833,
                "pt" : 83.33
            },{
                "ge" : 20834,
                "le" : 25000,
                "pt" : 104.16
            },{
                "ge" : 25001,
                "le" : 29166,
                "pt" : 125
            },{
                "ge" : 29167,
                "le" : 33333,
                "pt" : 150
            },{
                "ge" : 33334,
                "le" : 37500,
                "pt" : 175
            },
            {
                "ge" : 37501,
                "le" : 41666,
                "pt" : 200
            }, 
            {
                "ge" : 41667,
                "pt" : 208,
                "extra": {
                    "month": "Dec",
                    "pt": 212
                }
            }
        ]
        },

        "Karnataka": {
            "State": "Karnataka",
            "type": "gross",
            "frequency": 1,
            "bands": [{
                "ge": 0,
                "le": 15000,
                "pt": 0
            }, {
                "ge": 15001,
                "pt": 200
            }]
        }
    ,
    
        "Telangana": {
            "State": "Telangana",
            "type": "gross",
            "frequency": "monthly",
            "bands": [{
                    "ge": 0,
                    "le": 15000,
                    "pt": 0
                }, {
                    "ge": 15001,
                    "le": 20000,
                    "pt": 150
                },
                {
                    "ge": 20001,
                    "pt": 200
                }
            ]
        }
    ,
    
        "TN": {
            "State": "TN",
            "type": "gross",
            "frequency": 6,
            "bands": [{
                    "ge": 0,
                    "le": 21000,
                    "pe": 0
                }, {
                    "ge": 21001,
                    "le": 30000,
                    "pt": 100
                },
                {
                    "ge": 30001,
                    "le": 45000,
                    "pt": 235
                }, {
                    "ge": 45001,
                    "pt": 760
                }
            ]
        }
    ,
    
        "MH": {
            "State": "MH",
            "type": "gross",
            "frequency": 1,
            "bands": [{
                    "ge": 0,
                    "le": 7500,
                    "pt": 0
                }, {
                    "ge": 7501,
                    "le": 10000,
                    "pt_male": 175,
                    "pt_female": 0
                },
                {
                    "ge": 10001,
                    "pt": 200,
                    "extra": {
                        "month": "Feb",
                        "pt": 300
                    }
                }
            ]
        }
    ,
    
        "Punjab": {
            "State": "Punjab",
            "type": "taxable",
            "frequency": 1,
            "bands": [{
                "ge": 0,
                "le": 250000,
                "pt": 0
            }, {
                "ge": 250001,
                "pt": 200
            }]
        }
    
    }