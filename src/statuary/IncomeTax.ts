import { Employee } from "../Components/Employee";
import { LegalEntity } from "../Components/LegalEntity";
//import { Perquisite } from "../Components/Perquisite";
import { Loan } from "../Components/Loan";
import { Util } from "../utils/util";
import * as S from "./StatutoryConfig"
import * as C from "../utils/Config"
import { ITRules, SurchargeRules, Cess } from "./incomeTaxRules"
import { SalaryDescription } from "../Components/SalaryDescription";
import { ItSheetDescription } from "../Components/ItSheetDescription";
import { SectionTenRules } from "./SectionTenRules"
//import { runInThisContext } from "vm";
import { InvestmentPlanComponent } from "../Components/InvestmentPlanComponent";
import { InvestmentComponentType } from "../Components/InvestmentComponentType";
import { SectionSixARules } from "./SectionSixARules";
import { MasterSalary } from "../../src/Components/MasterSalary";
import { HRA } from "../../src/Components/HRA";
import { ItSheet } from "../../src/Components/ItSheet";
import { PfRules } from "./PfRules";
//import { emitKeypressEvents } from "readline";


export class IncomeTax {


    legalEntity: LegalEntity
    employee: Employee
    //perquisite: Perquisite
    month: Date
    sbiInteresRate: {
        [key: string]: number;
    };
    masterSalary: MasterSalary
    itSheet: ItSheet
    investmentPlanComponentTypes: { [key: string]: InvestmentComponentType }
    projectedPf
    projectedVpf
    pfRules : PfRules

    constructor(legalEntity, employee, month, investmentPlanComponentTypes, sbiInterestRate , pfRules) {
        this.legalEntity = legalEntity
        this.employee = employee
        this.month = month
        // this.itEarningDescription = {}
        // this.totalEarning = 0;
        this.investmentPlanComponentTypes = investmentPlanComponentTypes
        this.sbiInteresRate = sbiInterestRate
        this.pfRules = pfRules 
    }


    calculateTax(masterSalary: MasterSalary) {
        this.projectedPf = 0
        this.projectedVpf = 0
        this.masterSalary = masterSalary
        this.itSheet = masterSalary.itSheet
        this.getPrevData()
        this.calculateProjected()
        this.calculateSection10Exemptions()
        this.calculateSectionSixteenDeduction()
        this.calculateTaxablePerk()
        //this.calculateOtherIncome()
        this.calculateSixADeduction()
        // check deductions under 6 A
        let taxableIncome = this.itSheet.calculateTaxableIncome()
        this.itSheet.incomeTax = this.calculateIncomeTax(taxableIncome)
        this.itSheet.surcharge = this.calculateTotalSurcharge(this.itSheet.taxableIncome , this.itSheet.incomeTax)
        this.itSheet.cess =  this.calculateCess(this.itSheet.incomeTax , this.itSheet.surcharge , this.itSheet.marginalRelief)
        this.itSheet.totalTax = this.calculateTotalTax(this.itSheet.incomeTax , this.itSheet.surcharge , this.itSheet.marginalRelief , this.itSheet.cess)
        this.itSheet.calculateTds(Util.getTaxableMonthRemaining(this.month))
    }


    getPrevData() {
        let lastSalary: MasterSalary = this.employee.salaryRegister.getLastSalary(Util.getMonthIndex(this.month))
        if (lastSalary != null) {
            console.log('==================Inside if earning :', lastSalary)            
            //end
            this.itSheet.getLastSheetData(lastSalary.itSheet)
            for (let key in lastSalary.itSheet.itEarningDescription) {
                let salComp = this.legalEntity.salaryComponents[key]
                this.masterSalary.itSheet.addPrevTaxableIncome(salComp, lastSalary.itSheet.itEarningDescription[key].prevMonth + lastSalary.itSheet.itEarningDescription[key].currentMonth)
            }
        }
    }

    calculateProjected() {
        let basic = 0
        let da = 0
        let hra = 0
        let food = 0
        let bonus = 0
        let gross = 0        
        let monthRemaining = Util.getTaxableMonthRemaining(this.month)
        //read Salary Structure and calculate on the months remaining
        if (monthRemaining > 0) {
            this.masterSalary.salaryStructure.salaryComponents.forEach(element => {
                switch (element.code) {
                    case C.Basic : basic = element.value
                    case C.DA : da = element.value
                    case C.HRA : hra = element.value
                    case C.Food : food = element.value
                    case C.Bonus : bonus = element.value
                }
                if (this.legalEntity.salaryComponentsID[element.code].isPartOfGross()){
                    gross += element.value
                }                
                if (this.legalEntity.salaryComponents[element.code].taxable) {
                    let value = element.value * monthRemaining
                    //let value = this.masterSalary.salaryStructure.getSalaryComponentProjectedValue(element.code, monthRemaining)
                    this.masterSalary.itSheet.addProjectedTaxableIncome(this.legalEntity.salaryComponents[element.code], value)
                }
            });
            this.projectedPf = this.pfRules.getMonthlyPf(basic , gross , bonus , hra , food, da) * monthRemaining
            this.projectedVpf = this.pfRules.getMonthlyVpf(this.masterSalary.salaryStructure.vpf , basic,gross,bonus , hra , food, da) * monthRemaining
        }
    }



    calculateSection10Exemptions() {
        if(!this.itSheet.sectionTenExemptions){
        for (let key in this.itSheet.sectionTenExemptions) {
          if (key == C.HRA) {
                this.calculateHRA()
                this.itSheet.assignSectionTenExemption(C.HRA, this.itSheet.hraValue.exemption)
                // check paid should be same 
                if (!this.itSheet.checkHraPaid()) {
                    throw new Error("Wrong HRA calculated")
                }
            }
            else if (key == C.LeaveEncashment) {
                this.itSheet.assignSectionTenExemption(C.LeaveEncashment, this.calculateLeaveEncashment(this.itSheet.sectionTenExemptions[key].paid))
            }
            else {
                this.itSheet.assignSectionTenExemption(key, this.processSection10Rule(key, this.itSheet.sectionTenExemptions[key].paid))
            }
        }
      }
    }

    calculateTaxablePerk(){
        if(this.masterSalary.perquisites != undefined){
        this.itSheet.perquisites.total +=  this.masterSalary.perquisites.total
        this.itSheet.perquisites.superannuationPerk += this.masterSalary.perquisites.superannuation
        let perkDeduction = this.itSheet.perquisites.superannuationPerk 
        if (perkDeduction > S.SuperAnnutuationPerkLimit){
            perkDeduction = S.SuperAnnutuationPerkLimit
        }
        this.itSheet.totalPerkValue = this.itSheet.perquisites.total - perkDeduction
      }
    }

    calculateHRA() {
        this.masterSalary.itSheet.hraValue.current = this.getCurrentHraExemption()
        //this.masterSalary.itSheet.hraValue.previous = this.getPrevHraExemption()
        this.masterSalary.itSheet.hraValue.projected = this.getProjectedHraExemption()
        this.masterSalary.itSheet.hraValue.exemption = this.masterSalary.itSheet.hraValue.current.exemption + this.masterSalary.itSheet.hraValue.projected.exemption + this.masterSalary.itSheet.hraValue.previous.exemption
    }

    getCurrentHraExemption(): HRA {

        let hraRecieved = this.masterSalary.itSheet.getMonthEarning(C.HRA)
        let basicSal = this.masterSalary.itSheet.getMonthEarning(C.Basic) + this.masterSalary.itSheet.getMonthEarning(C.DA)
        let rent = this.masterSalary.rent
        return this.calculateHRAExemption(rent, basicSal, hraRecieved)

    }

    getPrevHraExemption(): HRA {
        let lastSalary: MasterSalary = this.employee.salaryRegister.getLastSalary(Util.getMonthIndex(this.month))
        let hraObj = new HRA()
        if (lastSalary != null) {
            hraObj.allowedBasic = lastSalary.itSheet.hraValue.previous.allowedBasic + lastSalary.itSheet.hraValue.current.allowedBasic
            hraObj.exemption = lastSalary.itSheet.hraValue.previous.exemption + lastSalary.itSheet.hraValue.current.exemption
            hraObj.receieved = lastSalary.itSheet.hraValue.previous.receieved + lastSalary.itSheet.hraValue.current.receieved
            hraObj.rentMinusBasic = lastSalary.itSheet.hraValue.previous.rentMinusBasic + lastSalary.itSheet.hraValue.current.rentMinusBasic
        }
        return hraObj
    }

    getProjectedHraExemption(): HRA {
        let projectedBasic = this.masterSalary.itSheet.getProjectedEarning(C.Basic) + this.masterSalary.itSheet.getProjectedEarning(C.DA)
        let projectedRent = this.masterSalary.rent * Util.getTaxableMonthRemaining(this.month)
        let projectedHra = this.masterSalary.itSheet.getProjectedEarning(C.HRA)
        return this.calculateHRAExemption(projectedRent, projectedBasic, projectedHra)

    }

    calculateHRAExemption(rent, basicSal, hra): HRA {
        let hraObj = new HRA()
        hraObj.allowedBasic = S.NonMetroHraBasic * basicSal / 100
        if (this.masterSalary.metro) {
            hraObj.allowedBasic = S.MetroHraBasic * basicSal / 100
        }

        hraObj.receieved = hra
        hraObj.rentMinusBasic = Math.max(rent - S.BasicDeductionFromRent * basicSal / 100, 0)
        hraObj.exemption = Math.min(hraObj.allowedBasic, hraObj.receieved, hraObj.rentMinusBasic)
        if (hraObj.exemption < 0) { hraObj.exemption = 0 }
        return hraObj
    }

    calculateLeaveEncashment(paid: number) {
        let basicDa = this.masterSalary.itSheet.getAmountTillDate(C.Basic) + this.masterSalary.itSheet.getAmountTillDate(C.DA)
        let numberOfMonth = this.employee.getNumberOfWorkingMonths()
        if (numberOfMonth <= S.LeaveEncashmentMonthCheck) {
            basicDa = basicDa * numberOfMonth / numberOfMonth //numerator should be sum of last months
        }
        else {
            basicDa = basicDa * S.LeaveEncashmentMonthCheck / S.LeaveEncashmentMonthCheck // Numerator should ne last 10 months basic + Da
            //basicDa = this.itEarningDescription[C.Basic].currentMonth + this.itEarningDescription[C.DA].currentMonth            
        }
        return Math.min(basicDa, S.LeaveEncahmentMaximum, paid)
    }

    processSection10Rule(key: string, paid: number): number {
        let rule = SectionTenRules[key]
        var invComp: InvestmentPlanComponent = this.employee.investmentPlanComponent[rule.abbreviation]
        var approved = invComp.planned
        if (invComp.approved > 0) {
            approved = invComp.approved
        }
        if (rule.type == "declared") {
            approved = approved // this logic will change later as other settings will be added
        } else if (rule.type == "recieved") {
            approved = paid
        }
        else {
            approved = paid // see what will be the null case
        }

        var exemption = Math.min(approved, paid)
        if (rule.limit != null) {
            let limit = 0
            if (rule.limitType = 'Flat') {
                if (rule.limitPeriod = 'Annual') {
                    limit = rule.limit
                } else if (rule.limitPeriod = 'Monthly') {
                    limit = rule.limit * 12
                }
                else { throw new Error("Limit in Section 10") }
            }
            else {
                let multiplier = 0
                switch (rule.limitFactor) {
                    case "BaseDays": {
                        multiplier = this.employee.masterSalary.baseDays // this.legalEntity.getBaseDays()
                    }
                }
                limit = rule.Limit * multiplier
            }
            if (exemption > limit) {
                exemption = limit
            }
        }
        return exemption
    }


    calculateSectionSixteenDeduction() {
        if(this.itSheet.totalEarning != undefined){
            let value = Math.min(S.StandardDeductionValue, this.itSheet.totalEarning)
            this.itSheet.addSectionSixteenDeduction(S.StandardDeductionName, value)
            let ptPaidEst = this.masterSalary.pt.paidTillNow + this.masterSalary.pt.unpaidPT
            let ptDeduct = Math.min(ptPaidEst, S.maxPT)
            this.itSheet.addSectionSixteenDeduction(C.PT, ptDeduct)
        }
    }


    calculateSixADeduction() {
        this.getEmployeeSixAValues()
        this.itSheet.totalSectionSixADeduction =  this.processSixA()

    }

    processSixA() : number {
        let deduc = 0
        for (let section in this.itSheet.sectionSixAValues) {
            if (this.itSheet.sectionSixAValues[section].total < 0) {
                continue
            }
            let total = this.itSheet.sectionSixAValues[section].total
            let rule = SectionSixARules[section]
            if (total > rule.max) {
                this.itSheet.sectionSixAValues[section].allowed = rule.max
            }
            else {
                this.itSheet.sectionSixAValues[section].allowed = total
            }
            deduc += this.itSheet.sectionSixAValues[section].allowed
            if (rule.exclusive_group.length > 0) {
                rule.exclusive_group.forEach(element => {
                    if (this.itSheet.sectionSixAValues[element] != null && this.itSheet.sectionSixAValues[element].total > 0) {
                        throw new Error("Cant have mutually exclusive components")
                    }
                }
                );
            }
            if (rule.assert.length > 0) {
                deduc = deduc - this.itSheet.sectionSixAValues[section].allowed
            }
        }
        return deduc;
    }

    getEmployeeSixAValues() {
        let pfAdded = false
        let vpfAdded = false
        if(this.employee.investmentPlanComponents != null){
        this.employee.investmentPlanComponents.forEach(element => {
            let investmentComponentType: InvestmentComponentType = this.investmentPlanComponentTypes[element.investmentComponentTypeID]
            if (investmentComponentType.sectionName == S.SectionSixA) {
                let value = element.planned
                if (Util.getMonthIndex(this.month) >= S.cutoffMonthForInvestmentProof){ 
                        value = element.approved 
                }                
                if (investmentComponentType.abbreviation == C.PF){
                    this.itSheet.totalPfPaidTillNow += this.masterSalary.pf.employeeContrib
                    value = this.itSheet.totalPfPaidTillNow + this.projectedPf                        
                    pfAdded = true
                }
                else if (investmentComponentType.abbreviation == C.VoluntaryPF){                    
                    this.itSheet.totalVpfPaidTillNow += this.masterSalary.vpf
                    value = this.itSheet.totalPfPaidTillNow + this.projectedVpf                        
                    vpfAdded = true
                }
                this.itSheet.addSectionSixAValues(investmentComponentType.subGroup, investmentComponentType.name, value)
        }})}
        if (!pfAdded){
            this.itSheet.totalPfPaidTillNow += this.masterSalary.pf.employeeContrib
            let value = this.itSheet.totalPfPaidTillNow + this.projectedPf                        
            if (value > 0){
                this.itSheet.addSectionSixAValues(S.PfSubGroup, C.VoluntaryPF, value)
            }
        }
        if (!vpfAdded){            
            this.itSheet.totalVpfPaidTillNow += this.masterSalary.vpf
            let value = this.itSheet.totalPfPaidTillNow + this.projectedVpf                       
            if (value > 0){
                this.itSheet.addSectionSixAValues(S.PfSubGroup, C.VoluntaryPF, value)
            }
        }
    }

    calculateOtherIncome() {
        // this.employee.otherIncome.otherSources.forEach(element => {
        //     this.otherIncome.push({ name: element.name, value: element.value })
        // }); this.employee.otherIncome

    }

    calculateSection24() {
        // var lossFromHouseProperty = 0
        // var interestOnHousingLoan = 0
        // this.employee.otherIncome.letOutProperties.forEach(element => {
        //     lossFromHouseProperty += element.getIncome()
        // });
        // if (this.employee.otherIncome.selfOccupied != null) {
        //     interestOnHousingLoan += this.employee.otherIncome.selfOccupied.getIncome()
        // }
        // this.section24Deduction.InterestOnHousingLoan = interestOnHousingLoan
        // this.section24Deduction.LossOnHouseProperty = lossFromHouseProperty
        // this.section24Deduction.TotalDeduction = interestOnHousingLoan + lossFromHouseProperty
        // if (lossFromHouseProperty + interestOnHousingLoan < S.MaximumSection24Deduction * -1) {
        //     this.section24Deduction.TotalDeduction = S.MaximumSection24Deduction * -1
        // }
    }

    calculateIncomeTax(taxableIncome: number): number {
        if (taxableIncome <= 0) { return 0 }
        let tax = 0
        let calculationDone = true
        for (let i = 0; i < ITRules.length; ++i) {
            let calculationTried = false
            let match = true
            let checkMin = false
            let checkMax = false
            if (ITRules[i].ge) {
                checkMin = true
            }
            if (ITRules[i].le) {
                checkMax = true
            }
            if ((!checkMin || taxableIncome >= ITRules[i].ge) && (!checkMax || taxableIncome <= ITRules[i].le)) {
                tax = ITRules[i].fixed + Math.round((taxableIncome - ITRules[i].ge + 1) * ITRules[i].value / 100) - ITRules[i].rebate
                if (tax < 0) { tax = 0 }
            }
        }
        return tax
    }

    calculateTotalSurcharge(taxableIncome, incomeTax) {
        if (taxableIncome <= 0 || incomeTax <= 0) { return 0 }
        let surcharge = 0
        let calculationDone = true
        this.itSheet.marginalRelief = 0
        for (let i = 0; i < SurchargeRules.length; ++i) {
            let calculationTried = false
            let match = true
            let checkMin = false
            let checkMax = false
            if (SurchargeRules[i].ge) {
                checkMin = true
            }
            if (SurchargeRules[i].le) {
                checkMax = true
            }
            if ((!checkMin || taxableIncome >= SurchargeRules[i].ge) && (!checkMax || taxableIncome <= SurchargeRules[i].le)) {
                surcharge = Math.round(SurchargeRules[i].value * incomeTax / 100)
                let excessTax = incomeTax + surcharge - SurchargeRules[i].marginal
                let excessIncome = taxableIncome + 1 - SurchargeRules[i].ge
                if (excessTax > excessIncome) {
                    this.itSheet.marginalRelief = excessTax - excessIncome //take this out of this and get it in return for later
                }
            }
        }
        return surcharge
    }

    calculateCess(incomeTax, surcharge, marginalRelief) {
        return Math.round((incomeTax + surcharge - ((marginalRelief == undefined) ? 0 : marginalRelief)) * Cess / 100)
    }

    calculateTotalTax(incomeTax, surcharge, marginalRelief, cess) {
        return Math.round(incomeTax + surcharge - ((marginalRelief == undefined) ? 0 : marginalRelief) + cess)
    }
}

