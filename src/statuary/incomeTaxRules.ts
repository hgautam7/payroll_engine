export const ITRules = [
    { ge: 0, le: 250000, fixed: 0, value: 0, rebate: 0 },
    { ge: 250001, le: 500000, fixed: 0, value: 5, rebate: 12500 },
    { ge: 500001, le: 1000000, fixed: 12500, value: 20, rebate: 0 },
    { ge: 1000001, fixed: 112500, value: 30, rebate: 0 }

]

export const SurchargeRules = [    
    { ge: 5000001 , le: 10000000 , value:10 , marginal:1312500},
    { ge: 10000001 , le: 20000000 , value:15 , marginal:3093750},
    { ge: 20000001 , le: 50000000 , value:25 , marginal:6684375},
    { ge: 50000001 , value:37 , marginal:18515625 } 
]

export const Cess = 4