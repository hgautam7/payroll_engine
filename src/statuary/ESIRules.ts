import { SalaryDescription } from './../Components/SalaryDescription';
import { LegalEntity } from './../Components/LegalEntity';
import { Employee } from './../Components/Employee';
import { ESICSettings } from './../Components/ESICSettings';
import { ESIContribution } from './ESIRulesList';
import { SalaryComponent } from '../Components/SalaryComponent';
import * as S from "./StatutoryConfig"
import { Util } from '../../src/utils/util';
import { EmployeeESIC } from '../../src/Components/EmployeeESIC';


export class ESIRules {
    
    constructor() {
    }


    calculate(esicWage:EmployeeESIC , date:Date ,  prevMonthEsicWage: EmployeeESIC , city: string){
    //      move it outside no legal entity or employee specific code here
    //     if (!this.legalEntity.locations[this.employee.location.id].esiApplicable){
    //         this.esic ={wage:this.employee.salary.getEsicWage() , eligibility:false, employee:0,employer:0,check_next:true , defaultWage:this.employee.salary.getDefaultEsicWage()}
    //         return
    //    }
        
        if (this.eligibilityCheckRequired(esicWage , date , prevMonthEsicWage)) {
            esicWage.eligibility = this.checkEligibility(esicWage.applicabilityWage , date)
        }
        if (esicWage.eligibility){
            this.calculateESIC(esicWage , date, city)            
        }
        else {
            esicWage.checkNext = true
            esicWage.employeeContribution = 0
            esicWage.employerContribution = 0
        }
    }

    eligibilityCheckRequired(esicWage : EmployeeESIC , month:Date , prevMonthEsicWage : EmployeeESIC) : boolean {   
      
       if (S.ESICMonthForChecking.includes(Util.getMonth(month))) {
            return true
        } 
        if (prevMonthEsicWage == null || prevMonthEsicWage.checkNext){
            return true
        } 
        esicWage.eligibility = prevMonthEsicWage.eligibility
        esicWage.checkNext =prevMonthEsicWage.checkNext
        if (!prevMonthEsicWage.eligibility && prevMonthEsicWage.applicabilityWage > esicWage.applicabilityWage){
            return true
        } 
        else if (S.ESICWageIncreaseOverride) {
            return true
        }
        return false
    }

    //later make it so it returns based on month and year 
    checkEligibility(wage:number , month) : boolean {
        return (wage <= S.ESICWage)
    }
  

    calculateESIC(esicWage : EmployeeESIC , date:Date , city:String){
        esicWage.checkNext = false
        let esicRule = ESIContribution[this.getEmployeeEsiLocation(city)]
        esicWage.employerContribution = Util.getRoundOff(esicRule[S.ESICEmployerContrib]*esicWage.wage/100 ,S.ESICRound)
        esicWage.employeeContribution = Util.getRoundOff(esicRule[S.ESICEMployeeContrib]*esicWage.wage/100 ,S.ESICRound)
    }


    getEmployeeEsiLocation(city): string {
        if ( ESIContribution[city])
            return city
        else return "all"
    }
}