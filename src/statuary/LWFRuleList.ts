export const LWFRuleList = {
    "Karnataka": {
        "State": "Karnataka",        
        "frequency": "annual",
        "month" : "Nov",
        "employee" : 20,
        "employer" : 40,
        "wage" : "all"
    },  
     "Chandigarh": {
        "State": "Chandigarh",        
        "frequency": "halfYearly",
        "month" : ["sep", "mar"],
        "employee" : 5,
        "employer" : 20,
        "wage" : "all"
    },

    "Gujarat": {
        "State": "Chandigarh",        
        "frequency": "halfYearly",
        "month" : ["sep", "mar"],
        "employee" : 5,
        "employer" : 20,
        "wage" : "all"
    },

    "Andaman & Nicobar Islands":{
        "State": "Andaman & Nicobar Islands",        
        "frequency": "monthly",
        "month" : ["sep", "mar"],
        "employee" : 5,
        "employer" : 20,
        "wage" : "all"
    },

"Telangana": {
    "State": "Telangana",        
    "frequency": "annual",
    "month" : "dec",
    "employee" : 10,
    "employer" : 30,
    "wage" : 3500
}
,

"Andhra Pradesh": {
    "State": "Andhra Pradesh",        
    "frequency": "annual",
    "month" : "dec",
    "employee" : 30,
    "employer" : 70,
    "wage" : 1600
}
,

"kerela": {
    "State": "kerela",        
    "frequency": "monthly",
    "month" : "all",
    "employee" : 20,
    "employer" : 20,
    "wage" : "all"
}
,
"MH": {
    "State": "MH",        
    "frequency": "monthly",
    "month" : "all",
    "employee" : 10,
    "employer" : 20,
    "wage" : "all"
},

"Arunachal Pradesh": {
    "State": "AR",        
    "frequency": "monthly",
    "month" : "all",
    "employee" : 10,
    "employer" : 20,
    "wage" : "all"
}
,
"Bihar": {
    "State": "BH",        
    "frequency": "monthly",
    "month" : "all",
    "employee" : 10,
    "employer" : 20,
    "wage" : "all"
}
,

"MP": {
    "State": "MP",        
    "frequency": "halfYearly",
    "month" : ["dec" , "jun"],
    "bands": [{
        "ge": 0,
        "le": 3000,
        "employee": 6,
        "employer" : 18
    }, {
        "ge": 3001,
        "le": 3500,
        "employee": 12,
        "employer" : 36
    },
    {
        "ge": 3501,
        "employee": 0,
        "employer" : 0
    }
]
}
}