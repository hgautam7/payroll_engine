export const PayRollStartMonth = 'Apr'



export const maxPT = 2500
export const sixthMonthlyPTLastInitiate = 10
export const sixthMonthlyPTLastCheck = 11
export const sixthMonthlyPTFirstInitiate = 4
export const sixthMonthlyPTFirstCheck = 5
export const ptTaxableSalaryType = 'taxable'
export const ptSalaryTypeGross = 'gross'


// PF Config
export const EPSEmployeeAge = 58
export const maxEPSWage = 15000
export const EPSPercentage = 8.33
export const EmployeePfPercentage = 12
export const EmployerPFPercentage = 12 // hemant : question to saurabh : 8.33% goes to pension fund and 3.67% to pf contri.
                                       // .pension fund is missing ?
export const RestrictedPfWage = 15000
export const PfSubGroup = '80C'

export const PFAdministrationChargeGovtPercentage = 0.5
export const PFAdministrationChargeSelfPercentage = 0
export const PFAdministartionName = "PF Admin Charge"
export const PFAdministartionAbbreviation = "PF Admin Charge"
export const PFInspectionChargesSelfPercentage = 0.18
export const PFInspectionChargeName = "PF Inspection Charge"
export const PFInspectionChargeAbbreviation = "PF Inspection Charge"
export const EdliContributionPercentage = 0.5 * this.epsWage / 100
export const EdliInspectionChargesPercentage = .005

//ESIC
export const ESICMonthForChecking = ["Apr","Oct"]
export const ESICName = "esic"
export const ESICAbbreviation = "esic"
export const ESICWage = 21000
export const ESICWageIncreaseOverride = false
export const ESICEmployerContrib = "Employer_Contrib"
export const ESICEMployeeContrib = "Employee_Contrib"
export const ESICRound = "RoundUp"

//HRA

export const MetroHraBasic = 50
export const NonMetroHraBasic = 40
export const BasicDeductionFromRent = 10

// IT Settings

export const SectionTen = "Section 10"
export const SectionTenTypeCode = "code"
export const SectionTenTypeRecieved = "receieved"
export const LeaveEncashmentMonthCheck = 10 
export const LeaveEncahmentMaximum = 300000
export const StandardDeductionName = "Standard Deduction"
export const StandardDeductionValue = 50000
export const cutoffMonthForInvestmentProof = 10
export const SectionSixA = "Section 6A"
export const SuperAnnutuationPerkLimit = 100000

//Other Income
export const MaximumSection24Deduction = 200000
