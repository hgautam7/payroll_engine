import { PtRuleList as PtRuleList } from "./PtRuleList"
import { Employee } from "../Components/Employee";
import { LegalEntity } from "../Components/LegalEntity";
import { MasterSalary } from "../Components/MasterSalary";
import {Location} from "../Components/Location"
import * as S from "./StatutoryConfig"
import { PT } from "../utils/Config"
import { Util } from "../utils/util";
import { EmployeePT } from "../../src/Components/EmployeePT";

export class PtRules {

    readonly maxPT = S.maxPT

    legalEntity: LegalEntity
    //employee: Employee
    ptSalary
    //wage : number
    paid_till_now
    state
    value
    date : Date
    pt
    //ptMale
    //ptFemale
    ptRule
    unpaidPt

    static ptHash = {}
    constructor(legalEntity , date : Date) {
        this.legalEntity = legalEntity
        this.date = date
    }

    calculate(currentPt : EmployeePT , ptList : {[key:number] : EmployeePT } , sex : string , deductMonthly : boolean ) {         
     /* if (currentPt.applicable) {
          currentPt.value = 0
          currentPt.unpaidPT = 0
          return
      }*/
      this.ptRule = PtRuleList[currentPt.stateCode]
        // check added-arushi
      if(this.ptRule){  
          // end
        let ptSalaryType = this.ptRule.type
        let ptFrequency = this.ptRule.frequency
        if (ptFrequency > 1) {
            if (ptFrequency == 6) {                
                this.calcultePtWageSixMonthly(currentPt , ptList , sex , deductMonthly)
            }
            else {
                throw Error("Invalid Condition")
            }
        } else {       
          currentPt.value = this.processPt(currentPt.wage , sex , currentPt.stateCode)
          currentPt.unpaidPT = 0
          currentPt.paidTillNow += currentPt.value
        }
        //this.checkMaximumPT()
     }   
    }

    public checkLastMonth(currentPt : EmployeePT , lastMonthPt : EmployeePT){
        if (lastMonthPt != null && currentPt.stateCode != lastMonthPt.stateCode && lastMonthPt.unpaidPT > 0) {
            currentPt.earlierPT = new EmployeePT()
            currentPt.earlierPT.applicable = true
            currentPt.earlierPT.value = lastMonthPt.unpaidPT
            currentPt.earlierPT.unpaidPT = 0
            currentPt.earlierPT.stateCode = lastMonthPt.stateCode
            currentPt.earlierPT.stateId = lastMonthPt.stateId
            currentPt.earlierPT.earlierPT = null
            currentPt.earlierPT.wage = 0    
            currentPt.paidTillNow += lastMonthPt.unpaidPT
        }    
    }

    checkMaximumPT(pt) : boolean{
        return (pt > this.maxPT)
        // let paidTill = 0
        // let prevMonthEmpSalary = this.employee.getPrevMonthMasterSalary(this.month)
        // if(prevMonthEmpSalary) paidTill = prevMonthEmpSalary.pt.paidTillNow
        // if (paidTill >= this.maxPT) {
        //      this.pt = 0
        //     this.paid_till_now = paidTill
        // }
        // else if ((paidTill + this.pt) > this.maxPT) {
        //     this.pt = this.maxPT - paidTill
        //     this.paid_till_now = this.maxPT
        // }
        // else {
        //     this.paid_till_now = paidTill + this.pt
        // }
    }

    processPt(wage:number , sex , ptStateCode:string):number {
        let ptMale = 0
        let ptFemale = 0
        let bands = PtRuleList[ptStateCode] 
        console.log("BANDS = ",bands.bands)
        for (let i = 0; i < bands.bands.length; ++i) {
            let calculationTried = false
            let match = true
            console.log("GE = ",bands.bands[i].ge)
            if (bands.bands[i].ge){
                match = (bands.bands[i].ge <= wage)
                calculationTried = true
            }
            if (bands.bands[i].le){
                match = (bands.bands[i].le >= wage)
                calculationTried = true
            }
            if (match && calculationTried) {
                if (bands.bands[i].extra) {
                    if (bands.bands[i].extra.month == Util.getMonth(this.date)) {
                        ptMale = bands.bands[i].extra.pt
                        ptFemale = bands.bands[i].extra.pt
                    }
                    else {
                        ptMale = (bands.bands[i].ptMale) ? bands.bands[i].ptMale : bands.bands[i].pt
                        ptFemale = (bands.bands[i].ptFemale) ? bands.bands[i].ptFemale : bands.bands[i].pt
                    }
                }
                else {
                    ptMale = (bands.bands[i].ptMale) ? bands.bands[i].ptMale : bands.bands[i].pt
                    ptFemale = (bands.bands[i].ptFemale) ? bands.bands[i].ptFemale : bands.bands[i].pt
                }
                return (sex == "female") ? ptFemale : ptMale
            }
        }
        throw new Error("PT Wrong")
    }


    calcultePtWageSixMonthly(currentPt : EmployeePT , ptList : {[key:number] : EmployeePT } , sex:string , deductMonthly : boolean) {        
        let monthInt = Util.getMonthIndex(this.date)
        let state = ptList[monthInt].stateCode
        let initial = 0
        let ptWage = 0
        if (monthInt >=6) {initial = 6}
        let ptDeducted = 0
        let ptCalculated = 0
        for (let i = initial; i <= monthInt; ++i) {               
            let employeePT: EmployeePT =  ptList[i] 
                if (employeePT != null &&  employeePT.stateCode == state) {
                    ptWage += employeePT.wage
                    ptDeducted += employeePT.value
                }
                else if (employeePT.earlierPT != null && employeePT.earlierPT.stateCode == state) {
                        ptWage += employeePT.wage
                        ptDeducted += employeePT.value
                    }
                }                
            //this.wage += this.employee.salary.getGrossSalary() // get MarchProjected
        ptCalculated = this.processPt(ptWage , sex, state)        
        if (this.checkMaximumPT(ptCalculated)){
            ptCalculated = this.maxPT
        }
        if (deductMonthly){
            currentPt.unpaidPT = 0
            currentPt.value = ptCalculated - ptDeducted
            currentPt.paidTillNow += currentPt.value
        }   
        else if (monthInt == S.sixthMonthlyPTFirstCheck || monthInt == S.sixthMonthlyPTFirstInitiate || monthInt == S.sixthMonthlyPTLastCheck || monthInt == S.sixthMonthlyPTLastInitiate){
            currentPt.unpaidPT = 0
            currentPt.value = ptCalculated - ptDeducted
            currentPt.paidTillNow += currentPt.value
        }             
        else {
            currentPt.unpaidPT = ptCalculated - ptDeducted
            currentPt.value = 0
        }
    }

    // processExtra(band) {
    //     if (band.extra.month == Util.getMonth(this.date)) {
    //         this.ptMale = band.extra.pt
    //         this.ptFemale = band.extra.pt
    //     }
    //     else {
    //         this.ptMale = (band.ptMale) ? band.ptMale : band.pt
    //         this.ptFemale = (band.ptFemale) ? band.ptFemale : band.pt
    //     }
    // }

    // getPT(){
    //    return  {wage:this.wage, paidTillNow:this.paid_till_now, value :this.pt , state :this.state , unpaidPT:this.unpaidPt}
    // }
}