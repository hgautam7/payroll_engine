export const SectionSixARules = {
    "80C":  { display_group:"80C" ,  sub_group : "80C" , exclusive_group: [] , max:150000},
    "80D1": {display_group:"80D" , sub_group:"80D1" , exclusive_group:["80D2"], inclusive_group:["80D"], max:25000},
    "80D2": {display_group:"80D" , sub_group:"80D2" , exclusive_group:["80D1"], inclusive_group:["80D"],max:50000},
    "80D3": {display_group:"80D" , sub_group:"80D3" , exclusive_group:["80D4"], inclusive_group:["80D"],max:25000},
    "80D4": {display_group:"80D" , sub_group:"80D4" , exclusive_group:["80D3"], inclusive_group:["80D"],max:50000},
    "80D" : {display_group:"80D" , max:5000 , assert:["80D1" , "80D2"]},		
    "80EE" : {display_group:"80EE", sub_group:"80EE" , exclusive_group:["80EEA"] 	,max:50000},	
    "80EEA" :{display_group:"80EEA", sub_group:"80EEA" , exclusive_group:["80EE"] 	,max:150000}//	Interest on Home Loan (Availed during 2019-20 for the first house)	150000	Not to be provided if 80EE is opted for

}