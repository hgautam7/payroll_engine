export const SectionTenRules = {
"LA": { id:1,	subSection:"10AA",	name:"Leave Encashment" , abbreviation:"LE" , type:"code"},
"LTA":{id:2, subSection:"5", name:"Leave Travel Allowance", abbreviation:"LTA" , type:"declared"}, 
"Grt":{id:3, subSection:"10 i", name:"Gratuity" , abbreviation:"Grt",	type:"recieved", limit:200000 , limitType:"Flat" , limitPeriod:"annual"},
"HRA":{id:4, subSection:"13A", name:"House Rent Allowance" , abbreviation:"HRA", type:"code"},
"FC" :{id:5, subSection:"14" , name:"Food Coupon" , abbreviation:"FC" , type:"recieved", limit:100, limitType:"Dependant", limitFactor:"BaseDays"},
"CEA":{id:6, subSection:"14(2)" , name:"Children Education Allowance" , abbreviation:"CEA" , type:"Actual Amount Recieved" , limit:200 ,  limitType:"Flat" , limitPeriod:"monthly"},
/*7	10 (14 (2))	Hostel Fee Allowance	INR 300 per month for 2 children
{8	10 (14)	Telephone Reimbursement	Actual Amount Received or Declared which ever is least
{9	10 (14)	Internet Reimbursement	Actual Amount Received or Declared which ever is least
{10	10 (14)	Fuel & Maintenance Reimbursement	Actual Amount Received or Declared which ever is least
{11	10 (14)	Driver Salary Reimbursement	Actual Amount Received or Declared which ever is least
{12	10 (14)	Relocation Allowance	Actual Amount Received or Declared which ever is least
{13	10 (14 (2))	Uniform Allowance	Actual Amount Received or Declared which ever is least
{14	10 (14 (2))	           Development	Actual Amount Received or Declared which ever is least
{15	10 (14)	Gifts	Maximum INR 5000 PA
{16	10 (14)	Books & Periodicals	Actual Amount Received or Declared which ever is least
{17	10 (14)	Business Entertainment	Actual Amount Received or Declared which ever is least
{18	10 (10 B)	Retrenchment Compensation	Actual Amount Received or 5 Lacs which ever is least
{19	10 (10 C)	VRS Compensation	Actual Amount Received or 5 Lacs which ever is least
{20	10 (14(1))	Conveyance Allowance - Official	Actual Amount Received or Declared which ever is least
{21	10 (14 (2))	Helper Allowance	Actual Amount Received or Declared which ever is least
{22	10 (14 (2))	Research Allowance	Actual Amount Received or Declared which ever is least
{23	10 (14 (2))	Wardrobe Allowance	Actual Amount Received or Declared which ever is least
{24	10 (14 (2))	Transport Allowance	Actual Amount Received or INR 3200 which ever is least (Applicable only for Handicapped Employee)*/

}
