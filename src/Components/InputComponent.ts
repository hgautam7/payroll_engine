import * as C from "../utils/Config"
import { SalaryDescription } from "./SalaryDescription"
import { Employee } from "./Employee"
import { LegalEntity } from "./LegalEntity"
import { Util } from "../../src/utils/util"

const InputComponentType = {
  Earning: 'earning',
  Deduction: 'deduction'
}

const InpitRegisterStatus = {
  Input: 'input',
  Processed: 'processed'
}


export class InputComponent extends SalaryDescription {
  id: string
  employeeId: string
  financialYear: string
  assignedDate: Date
  processingMonth: Date
  type: string
  status: string

  constructor(obj) {
    super(obj , obj.value)
    this.id = obj.id
    this.employeeId = obj.employee_id
    this.financialYear = obj.financial_year
    this.assignedDate = obj.assigned_date
    this.processingMonth = obj.processing_month
    this.type = obj.type
    this.status = obj.status
  }

  public getType(): string {
    switch (this.type) {
      case InputComponentType.Earning :
        return C.Earning
        case InputComponentType.Deduction :
          return C.Deduction
      default: throw new Error("invalid component type")
    }
  }

  checkProcessingMonth(date : Date) : boolean{
    if (Util.monthArr[this.processingMonth.getMonth()] == Util.monthArr[date.getMonth()] && this.processingMonth.getFullYear() == date.getFullYear()){
      return true
  }
    return false  
  }

  public getSalDescription(value) : SalaryDescription{
    return new SalaryDescription(this , value)
  }

  public getDescrition(): SalaryDescription {
    return new SalaryDescription(this , this.value)
  }
}