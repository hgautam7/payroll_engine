import { MasterSalary } from "./MasterSalary";
import { OtherIncome } from "./OtherIncome";
import { EmployeePT } from "./EmployeePT";

export class SalaryRegister {
    id : number
    employee_id : number
    legal_entity_id : number
    previousEmployer : {income: number, pf: number, pt: number, tds: number}
    otherIncome: OtherIncome
    masterSalaryList : {[key: number] : MasterSalary} // 0 for apr , 1 may and so on  

    constructor(obj : any){
        this.id  = obj.id
        this.employee_id = obj.employee_id
        this.legal_entity_id = obj.legal_entity_id
        this.previousEmployer = obj.previousEmployer
        this.masterSalaryList = obj.masterSalaryList
    }

    getLastSalary(month:number){
        for (let i = month - 1 ; i >=0 ; --i){
            if (this.masterSalaryList[i]!= null) {return this.masterSalaryList[i]}
        }
        return null
    }
    //{[key:number] : EmployeePT }
    getPtList() : {[key:number] : EmployeePT}{
        let list = {}
        for(let key in this.masterSalaryList){
            list[key] = this.masterSalaryList[key].pt                
        }
        return list
    }


}