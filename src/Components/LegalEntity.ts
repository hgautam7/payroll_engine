import { SalaryComponent } from "./SalaryComponent";
import {ESICSettings} from "./ESICSettings";
import { LePFSettings } from "./LePFSettings";
import {Location} from "./Location"
import { PTSettings } from "./PTSettings";
import { SalaryStructure } from "./SalaryStructure";
import { State } from "./State";

export class LegalEntity {
    id               : number  
    name             : string
    pfSettings       : LePFSettings
    //esicSettings     : ESICSettings move to Location
    staturyBonus     : any
    //ptSettings       :{[key:string] : PTSettings} moved to Location
    locations        :{[key:string] : Location}
    // array or json?
    salaryComponents : {[key:string] : SalaryComponent} // abbreviation to SalaryComp
    salaryComponentsID : {[key:string] : SalaryComponent} // id to salary Comp
    salaryStructures : {[key:string] : SalaryStructure}
    
    baseDays         : any
    gratuitySettings : any
    
    stateList : {[key:string] : State}

    constructor(obj :any){
        this.id = obj.id
        this.name = obj.name
        this.pfSettings = obj.pfSettings 
        this.staturyBonus = obj.staturyBonus
        this.locations = obj.locations
        this.salaryComponents = obj.salaryComponents
        this.baseDays = obj.baseDays 
        this.salaryStructures = obj.salaryStructures
        this.salaryComponentsID = obj.salaryComponentsID
    }

     //was giving error legalentity.Locaiotns is not a fucntion
}
