export class EmployeePF {
    wage:number
    employeeContrib:number
    employerContrib:number
    epsWage:number
    eps:number
    edliContribution:number
    edliInspectionCharges:number
    administrationCharge:number
    pfInspectionCharges:number
    applicable:boolean     

    constructor(obj: any) {
        this.applicable = obj.pf_enabled
    }

}