import { HRA } from "./HRA";
import { SalaryDescription } from "./SalaryDescription";
import { ItSheetDescription } from "./ItSheetDescription";
import { SalaryComponent } from "./SalaryComponent";
import * as S from "../statuary/StatutoryConfig"


export class ItSheet {
    tdsOveride: number
    totalTax: number
    incomeTax: number
    surcharge: number
    cess: number
    marginalRelief: number
    hraValue: {
        exemptionActualPrevMonth: number,
        exemptionProjected: number,
        rentPaidMinusBasic: number,
        hraRecieved: number,
        hraAllowedBasic: number,
        hraExemption: number,
    }

    earnings
    inputTds: number
    tds: number
    educationCess: number
    totalTaxProjected: number
    tdsPaidTillMonth: number
    TdsPerMonthProjected: number
    // keys present in database and needed for payslip generation.
    deductions_16: { total: number, description: [{ name: "", value: "" }] }
    deductions_via: { total: number, description: [{ name: "", value: "" }] }
    deductions: { total: number, description: [] }
    exemptions: { total: "", description: [{ name: "", abr: "", value: "" }] }
    tax_calculation: {}
    perquisites: { total: number, superannuation_perk: number, description: Array<SalaryDescription> }

    itEarningDescription: { [key: string]: ItSheetDescription }
    sectionTenExemptions: { [key: string]: { name: string, declare: any, exemption: number, paid: number } }
    sectionSixteenDeductions: [{ name: string, value: number }]
    sectionSixADescription: { [key: string]: { name: string } }
    section24Deduction: { LossOnHouseProperty: number, InterestOnHousingLoan: number, TotalDeduction: number }
    sectionSixAValues: { [key: string]: { total: number, allowed: number } }
    otherIncome: [{ name: string, value: number }]
    totalEarning: number
    totalSectionTenExemption: number
    totalSectionSixteenDeduction: number
    totalSectionSixADeduction: number
    hraEarning: number
    taxableIncome: number

    constructor() {

    }

    addThisMonthIncome(salDescription: SalaryComponent, value) {
        if (salDescription.isTaxable()) {
            if (this.itEarningDescription[salDescription.code] == null) {
                this.itEarningDescription[salDescription.code] = new ItSheetDescription(salDescription)
            }
            this.itEarningDescription[salDescription.code].currentMonth += value
            if (salDescription.itSection == S.SectionTen) {
                this.sectionTenExemptions[salDescription.code].paid += value
            }
        }
    }

    addPrevTaxableIncome(salDescription: SalaryComponent, value) {

        if (salDescription.isTaxable()) {
            if (this.itEarningDescription[salDescription.code] == null) {
                this.itEarningDescription[salDescription.code] = new ItSheetDescription(salDescription)
            }
            this.itEarningDescription[salDescription.code].prevMonth += value
            if (salDescription.itSection == S.SectionTen) {
                this.sectionTenExemptions[salDescription.code].paid += value
            }
        }
    }

    addProjectedTaxableIncome(salDescription: SalaryComponent, value) {

        if (salDescription.isTaxable()) {
            if (this.itEarningDescription[salDescription.code] == null) {
                this.itEarningDescription[salDescription.code] = new ItSheetDescription(salDescription)
            }
            this.itEarningDescription[salDescription.code].projected += value
            if (salDescription.itSection == S.SectionTen) {
                this.sectionTenExemptions[salDescription.code].paid += value
            }
        }
    }

    getMonthEarning(code : string){
        if (this.itEarningDescription[code] != null) {
            return this.itEarningDescription[code].currentMonth
        }
        return 0
    }

    getProjectedEarning(code : string) {
        if (this.itEarningDescription[code] != null) {
            return this.itEarningDescription[code].projected
        }
        return 0
    }

    getPreviousEarning(code : string) {
        if (this.itEarningDescription[code] != null) {
            return this.itEarningDescription[code].prevMonth
        }
        return 0
    }
}