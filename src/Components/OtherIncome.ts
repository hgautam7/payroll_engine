import * as C from "../utils/Config"

export class OtherIncome {
    otherSources :[{name:string , value:number , type:string , status:string}] //type will be saving bank interest, fd interest amd other income 
    letOutProperties : [LetOutProperty]
    selfOccupied : SelfOccupied    

    
}

export class LetOutProperty {
    name:string
    rent:number
    municipalTax:number
    unrealisedRent:number
    value:number
    interest:number
    status:string

    getIncome(){
        let netRent = this.rent + this.unrealisedRent- this.municipalTax
        let stdDeduction = 0
        if (netRent > 0) {
            stdDeduction = Math.round(C.StandardDeductionOnRent * netRent / 100)
        }
        return netRent - stdDeduction - this.interest
    }
}

export class SelfOccupied {
    interest:number
    exemption : number
    
    getIncome(){
        return -this.interest
    }
}
