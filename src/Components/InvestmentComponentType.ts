import { NegativeSalaryName } from '../utils/Config';
export class InvestmentComponentType{
  id : string
  name : string
  abbreviation : string
  maximumAmount : number
  description : string
  sectionName : string
  subSection : string
  subGroup : string


  constructor(obj){
      this.name = obj.name
      this.abbreviation = obj.abbreviation
      this.maximumAmount = obj.maximumAmount
      this.description = obj.description
  }
}