/***
 * @author           : Saurabh
 * @Creation_Date    : 16th Oct 2019
 * @Description      : Master Salary Component 
 * 
 * @Modification_Date : 21st Oct 2019 
 * @Modified_By       : Hemant 
 * @Description       : Initialized LOP in constructor & added custom =  true [temp value], as not implemented in UI
 * 
 */

import { Util } from "../../src/utils/util";
import * as C from "../utils/Config";
import { Employee } from "./Employee";
import { EmployeeESIC } from "./EmployeeESIC";
import { EmployeeLWF } from "./EmployeeLWF";
import { EmployeePF } from "./EmployeePF";
import { EmployeePT } from "./EmployeePT";
import { EmployeeSalary } from "./EmployeeSalary";
import { InputComponent } from "./InputComponent";
import { ItSheet } from "./ItSheet";
import { LegalEntity } from "./LegalEntity";
import { LOP } from "./LOP";
import { NonCtCComponent } from "./NonCtCComponent";
import { SalaryArrears } from "./SalaryArrears";
import { SalaryComponent } from "./SalaryComponent";
import { SalaryDescription } from "./SalaryDescription";

export class MasterSalary {
  // this will come from Handler no need to add this while initiating 
  employee: Employee
  legalEntity: LegalEntity

  key:string //yymm
  month: Date
  metro: boolean
  rent: number
  baseDays: number
  salaryStructure: EmployeeSalary
  lop: { total: number, dates: Array<LOP> }
  rlop: { total: number, dates: Array<LOP> }
  netSalary: number
  nonCtcComponents: Array<SalaryDescription> = []
  inputComponents: Array<SalaryDescription> = []
  pt: EmployeePT
  pf: EmployeePF
  vpf : number
  totalVpfPaidTillNow : number
  esic: EmployeeESIC
  totalEarning: number
  totalDeduction: number
  earnings: { gross: number, description: Array<SalaryDescription> }
  deductions: { total: number, description: Array<SalaryDescription> }
  perquisites : {total: number , superannuation : number ,description : Array<SalaryDescription>}
  inputTax: number
  arrears: { [key: string]: Array<SalaryDescription> }
  nonCtcArrears: { [key: string]: Array<SalaryDescription> }
  inputArrears: { [key: string]: Array<SalaryDescription> }
  lwf: EmployeeLWF
  itSheet: ItSheet

  components = {} as { [key: string]: SalaryDescription }

  
  constructor() {
    this.lop={"total":0, dates: []} //added by hemant
    // this.lop.total = 0
    // this.month = obj.month
    // this.metro = obj.metro
    // this.vpf = obj.vpf
    // this.esic = obj.esic
    // this.netSalary = obj.netSalary
    // //this.perquisites = obj.perquisites
  }

  init() {
    this.pt = new EmployeePT()
    this.pt.stateId = this.employee.location.ptStateId
    this.pt.stateCode = this.legalEntity.stateList[this.pt.stateId].code
    this.pt.applicable = this.employee.location.ptApplicable
    this.esic = new EmployeeESIC()
    this.esic.applicable = (this.employee.location.esiApplicable && this.employee.esicApplicable)
    this.lwf = new EmployeeLWF()
    this.lwf.lwfApplicable = this.employee.location.lwfApplicable
    this.lwf.wwfApplicable = this.employee.location.wwfApplicable
    this.lwf.state = this.employee.location.getLwfState()
  }

  assignParents(employee: Employee, legalEntity: LegalEntity) {
    this.employee = employee
    this.legalEntity = legalEntity
  }

  initLopDays() {
    this.lop.total = this.initDays(this.lop)
  }

  initRlopDays() {
    this.rlop.total = this.initDays(this.rlop)
  }

  private initDays(obj) {
    let tot = 0
    obj.dates.forEach(element => {
      tot += element.deductionRaio
    });
    return tot
  }

  getGross() {
    return this.earnings.gross
  }

  getComponentValue(key:string):number{
    if (this.components[key] != null && this.components[key].value != null){
        return this.components[key].value
    }
    return 0
}

  insertPerk(salComp: any , value :number)
  insertPerk(salComp: SalaryComponent , value :number){
    if (this.perquisites.description == null){
      this.perquisites.description = new Array()
    }
    let salDes = new SalaryDescription(salComp , value)
    this.perquisites.description.push(salDes)
  }


  processSalaryStructure() {
    this.salaryStructure.custom = false // ctx.params.body.custom -- this is temp  value ,  it should come from UI employee salaryStructure
    if (this.salaryStructure.custom) {
      this.salaryStructure.generateSalaryComponents(this.legalEntity, this.month)
    }
    
    this.salaryStructure.salaryComponents.forEach((salaryDescription: SalaryDescription) => {
      let salaryComponent: SalaryComponent = this.legalEntity.salaryComponentsID[salaryDescription.code]
      let value = salaryComponent.calculate(salaryDescription.value, this.getAttendanceRatio())
     
      this.addESIApplicableWage(salaryComponent, salaryDescription.value)
      this.addESIWage(salaryComponent, value)
      this.addPtWage(salaryComponent, value)
      
      this.assignComponent(salaryComponent, value)
      this.addTaxableIncome(salaryComponent , value)
      /* if (salaryComponent.paid) {
        console.log('value ', value)
        //employee.salary.assignComponent(salaryComponent, value);
      } */
    })
    //employee.salary.addEsicWage(salaryComponent, salaryDescription.value , value)

  }

  addComponent(salaryDescription: SalaryDescription) {
    let value = salaryDescription.value
    if (this.components[salaryDescription.code] != null) {
      salaryDescription.value += this.components[salaryDescription.code].value
    }
    this.components[salaryDescription.code] = salaryDescription
  }

  assignComponent(salaryComponent: SalaryComponent, value: number) {
    let salaryDescription = new SalaryDescription(salaryComponent, value)
    if (salaryComponent.isEarning()) {
      this.addEarning(salaryComponent, value)
    }
    else if (salaryComponent.isDeduction()) {
      this.addDeduction(salaryComponent, value)
    }
    else if (salaryComponent.type == C.EmployerContribution) {
      //see what all components come here
    }
    /* else { // Temporarly commmented as UI data is marked incorrect 
      throw new Error("unknown component")
    } */
  }


  // insertComponent(salaryDescription : SalaryDescription , type : string){
  //     let value = salaryDescription.value
  //     if (this.components[salaryDescription.abbreviation] != null){
  //         salaryDescription.value += this.components[salaryDescription.abbreviation].value
  //     }
  //     this.components[salaryDescription.abbreviation] = salaryDescription
  //     if (type == C.Earning){
  //         this.earnings[salaryDescription.abbreviation] = salaryDescription
  //         this.earnings['gross'] +=value
  //     }
  //     else if (type == C.Deduction){
  //         this.deductions[salaryDescription.abbreviation] = salaryDescription
  //         this.deductions['gross'] +=value
  //     }
  //     else if (type == C.EmployerContribution){
  //         this.employerContribution[salaryDescription.abbreviation] = salaryDescription
  //         this.employerContribution['gross'] +=value
  //     }
  //     else {
  //         throw new Error("unknown component")
  //     }
  // }


  addDeduction(salComponent: SalaryComponent, value) {
    let salDescription = null
    if(salComponent.type == "DEDUCTION"){
      salDescription = new SalaryDescription(salComponent, value)
      this.deductions.description.push(salDescription)
      this.deductions.total += value
    }
  }



  addEarning(salComponent: SalaryComponent, value) {
    let salDescription = null
     if(salComponent.type == "EARNING" && salComponent.paid){
      salDescription = new SalaryDescription(salComponent, value)
      this.earnings.description.push(salDescription)
     // console.log("inside deduction arr = ",this.deductions)
      if (salComponent.isPartOfGross()) {
        this.earnings.gross += value
      }
    } 
  }
  
  
  addPtWage(salDescription: SalaryComponent, value) {
    if (salDescription.isPartOfPtWage()) {
      this.pt.wage += value
    }
  }

  addESIWage(salDescription: SalaryComponent, value) {
    if (salDescription.isPartOfESIDeduction()) {
      this.esic.wage += value
    }
  }

  addESIApplicableWage(salDescription: SalaryComponent, value) {
    if (salDescription.isPartofESIEligibility()) {
      this.esic.applicabilityWage += value
    }
  }


  addTaxableIncome(salDescription : SalaryComponent , value){
    this.itSheet.addThisMonthIncome(salDescription , value)
  }

  public getAttendanceRatio() {
    // console.log('attanedance ratio',baseDays)
    if (this.baseDays != 0) {
      return (this.baseDays - this.lop.total + this.rlop.total) / this.baseDays
    }
    return 0
  }

  public processNonCtcComponents(inputRegister: Array<NonCtCComponent>) {
    try {
      //console.log('***********non ctc*********', inputRegister)
      if (inputRegister.length > 0) {
        inputRegister.forEach((nonCtcComponent: NonCtCComponent) => {
          if (nonCtcComponent.checkApplicable(this.month)) {
            let salComp = this.legalEntity.salaryComponentsID[nonCtcComponent.code]
            let value = nonCtcComponent.getValue(this.month)
            this.nonCtcComponents.push(nonCtcComponent.getSalDescription(value))
            this.assignComponent(salComp, value)
            if (nonCtcComponent.checkForArrears(this.month)) {
              this.processNonCtcArrear(nonCtcComponent)
            }
          } 
        });
      }
    }
    catch (err) {
      console.log('Error from processInputComponents', err);
      throw new Error(err);
    }
  }

  processNonCtcArrear(nonCtcComponent: NonCtCComponent) {
    let noOfMonth = Util.getMonthDiff(nonCtcComponent.from, this.month)
    let processDate: Date = nonCtcComponent.from
    let salComp = this.legalEntity.salaryComponentsID[nonCtcComponent.salaryComponenyid]
    while (!(processDate.getFullYear() == this.month.getFullYear() && processDate.getMonth() == this.month.getMonth())) {
      let value = nonCtcComponent.getArrearValue(processDate)
      let key = this.employee.getKey(processDate)
      let salDesc = new SalaryDescription(salComp, value)
      if (this.nonCtcArrears[key] == null) {
        this.nonCtcArrears[key] = new Array<SalaryDescription>()
      }
      this.nonCtcArrears[key].push(salDesc)
      processDate.setMonth(1 + processDate.getMonth())
    }
  }

  public processInputComponents(inputRegister: Array<SalaryDescription>) {
    try {
     // console.log('***********input componets*********', inputRegister)
      if (inputRegister.length > 0) {
        inputRegister.forEach((inputComponent: InputComponent) => {
          if (inputComponent.checkProcessingMonth(this.month)) {
            let salComp = this.legalEntity.salaryComponentsID[inputComponent.code]
            if (salComp.code == C.TDS){
              this.inputTax += inputComponent.value
            }
            this.assignComponent(salComp, inputComponent.value)
            this.addPtWage(salComp, inputComponent.value)
            this.addESIWage(salComp, inputComponent.value)
            this.addESIApplicableWage(salComp, inputComponent.value)
            this.addTaxableIncome(salComp , inputComponent.value)
          }
        });
      }
    }
    catch (err) {
      console.log('Error from processInputComponents', err);
      throw new Error(err);
    }
  }

  public getPrevMonthSalary(){
    return this.employee.getPrevMonthSalary(this.month)
  }

  public processNegativeSal(){
    let prevMonthSal = this.getPrevMonthSalary()  
    if (prevMonthSal < 0) {
        let salComp = this.legalEntity.salaryComponents[C.NegativeSalaryAbbreviation]
        this.assignComponent(salComp , Math.abs(prevMonthSal))
      }
  }

  finalizeSalary(){
    
  }

  addArrears(date : Date , salaryArrears : SalaryArrears){
    let key = this.employee.getKey(date)
    if (this.arrears[key] == null){
      this.arrears[key] == salaryArrears
    } else {
    }
  }

  copyForArrears(earlierSalary : MasterSalary ){
    if (earlierSalary == null){return}
    this.lop = earlierSalary.lop
    this.rlop = earlierSalary.rlop
    this.rent = earlierSalary.rent
    this.baseDays = earlierSalary.baseDays
    this.processInputComponents(earlierSalary.inputComponents)
    this.addArrearNonCtcInputComponenets(earlierSalary.nonCtcComponents)        
    this.inputArrears = earlierSalary.inputArrears
    this.arrears = earlierSalary.arrears
    this.perquisites = earlierSalary.perquisites
  }

  private addArrearNonCtcInputComponenets(inputRegister){
    if (inputRegister) {
      inputRegister.forEach((nonCtcComponent: SalaryDescription) => {
          let salComp = this.legalEntity.salaryComponentsID[nonCtcComponent.salaryComponenyid]
          let value = nonCtcComponent.value
          this.nonCtcComponents.push(nonCtcComponent)
          this.assignComponent(salComp, value)          
        }
      );
    } 
  }

  getArrears(earlierSalary : MasterSalary) : SalaryArrears{
    let arrears = new Array<SalaryDescription>()  
    if (earlierSalary == null){

    }
    return null
  }
}