import { SalaryComponent } from "./SalaryComponent";

export class Perk  {

    id : string
    from : Date
    to : Date
    paymentMonthly : boolean
    amount : number
    custom : {}

}