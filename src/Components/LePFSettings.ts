export class LePFSettings {
    
    isApplicable: boolean;
    pfBody:string; //government or pf_trust
    employeeContributionRestrict: boolean; //restrict (true) or nonrestrict (false)
    employerContributionRestrict: boolean; //restrict (true) or nonrestrict (false)
    epsCalculationRestrict: boolean; //restrict (true) or nonrestrict (false)
    edliChargesApplicable: boolean;
    edliInspectionChargesPartOfCtc : boolean
    adminChargesPartOfCtc : boolean

    constructor(obj){
        this.isApplicable= obj.isApplicable
        this.pfBody= obj.pfBody; //government or pf_trust
        this.employeeContributionRestrict= obj.employeeContributionRestrict //restrict (true) or nonrestrict (false)
        this.employerContributionRestrict= obj.employerContributionRestrict //restrict (true) or nonrestrict (false)
        this.epsCalculationRestrict= obj.epsCalculationRestrict //restrict (true) or nonrestrict (false)
        this.edliChargesApplicable= obj.edliChargesApplicable
        this.edliInspectionChargesPartOfCtc = obj.edliInspectionChargesPartOfCtc
        this.adminChargesPartOfCtc = obj.adminChargesPartOfCtc
    }
}
