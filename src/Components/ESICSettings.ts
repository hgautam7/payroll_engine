export class ESICSettings {
    registered: boolean
    employeeContribution: number;
    employerContribution: number;
    allowOverridingEsic: boolean;
    employeeContributionOutsideGross: boolean;
    exclude_employer_share_from_esic_calculations: boolean;
    restrict_esic_gross_to_statuatory_gross: boolean;
    include_bonus_one_time_payment_for_eligiblity: boolean;
    include_bonus_one_time_payment_for_contribution: boolean;

    constructor(obj :any){
        this.registered = obj.registered
        this.employeeContribution = obj.employeeContribution
        this.employerContribution = obj.naemployerContribution
        this.employeeContributionOutsideGross = obj.employeeContributionOutsideGross 
        this.allowOverridingEsic = obj.allowOverridingEsic
        this.exclude_employer_share_from_esic_calculations = obj.exclude_employer_share_from_esic_calculations
        this.restrict_esic_gross_to_statuatory_gross = obj.restrict_esic_gross_to_statuatory_gross
        this.include_bonus_one_time_payment_for_eligiblity = obj.include_bonus_one_time_payment_for_eligiblity
        this.include_bonus_one_time_payment_for_contribution = obj.include_bonus_one_time_payment_for_contribution
    }
}
