export class EmployeeLWF {
    
    wage: number
    employerContribution: number
    employeeContribution : number
    total: number
    state: string
    stateId : string
    lwfApplicable : boolean
    wwfApplicable : boolean

    constructor(){
        this.wage = 0
        this.employerContribution = 0
        this.employeeContribution = 0
        
    }
}