import { SalaryComponent } from "./SalaryComponent";
import { SalaryDescription } from "./SalaryDescription";
import * as C from "../utils/Config"

export class Salary {
    ctc;
    earnings = {} as any;    
    deductions= {} as any;
    employerContribution= {} as any;
    components = {} as {[key:string] : SalaryDescription}
    esicDefaultWage = 0
    esicWage  = 0

    constructor(){
        this.earnings['gross'] = 0
        this.deductions['gross'] = 0

        this.employerContribution['gross'] = 0
        this.esicWage = 0
        this.esicDefaultWage = 0
    }

    
    getGrossSalary():number{
        return this.earnings['gross']
    }

    getComponentValue(key:string):number{
        if (this.components[key] != null && this.components[key].value != null){
            return this.components[key].value
        }
        return 0
    }


    // hemant
    getEarning(){
        return this.earnings
    }
    getDeduction(){
        return this.deductions
    }
  //end
    getTotalEarning(){
        return this.earnings['gross']
    }

    getTotalDeduction(){
        return this.deductions['gross']
    }

    getTotalEmployerContribution(){
        return this.employerContribution['gross']
    } 

    //making ESIC wage
    addEsicWage(salaryComponent: SalaryComponent , defaultValue : number , value: number){
        if (salaryComponent.considerEsiDeduction){
            this.esicWage += value
            this.esicDefaultWage += defaultValue
        }

    }

    getEsicWage(){
        return this.esicWage
    }

    getDefaultEsicWage(){
        return this.esicDefaultWage
    }

}

