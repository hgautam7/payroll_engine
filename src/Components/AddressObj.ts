export class AddressObj {
    
    address 
    city : string
    state : string
    stateID : string

    constructor(obj:any){
        this.address = obj
        this.city = (obj.cityID == undefined) ? obj.city :  obj.cityID.city_name
        this.state = (obj.stateID == undefined) ? obj.state :obj.stateID.state_name
        this.stateID = (obj.stateID.id == undefined) ? obj.stateID : obj.stateID.id
    }


}