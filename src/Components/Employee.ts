import { InvestmentComponentType } from './InvestmentComponentType';
import { EmployeeSalary } from './EmployeeSalary'
import { PayrollInput } from "./PayrollInput"
import { Attendance } from "./Attendance"
import { LegalEntity } from "./LegalEntity"
import { Salary } from './Salary';
import { SalaryDescription } from './SalaryDescription';
import { Loan } from './Loan';
import { SalaryRegister } from './SalaryRegister';
import { Util } from '../utils/util';
import * as C from '../utils/Config'
import { MasterSalary } from './MasterSalary';
import { InvestmentPlanComponent } from './InvestmentPlanComponent';
import { OtherIncome } from './OtherIncome';
import { Location } from '../Components/Location';
import { InputComponent } from './InputComponent';
import { Perk } from './Perk';
import { SalaryComponent } from './SalaryComponent';
import { TdsOverride } from './TdsOverride';

export class Employee {
    [x: string]: any;
    employee_id: string
    salaryStructure: EmployeeSalary
    salaryStructures: Array<EmployeeSalary> //sorted on basis of createdDate decreasing
    attendance: Attendance
    sex: string
    location: Location//this will be id
    age: number
    salaryRegister: SalaryRegister //later on {[key:number]:  SalaryRegister} // key is financial year we will have to read for all financial years 
    inputComponents: Array<InputComponent>//Array<SalaryDescription>
    ownerLegalEntity: LegalEntity
    salary: Salary
    arrears: number
    tdsDeducted: number
    tdsOverride: TdsOverride
    loans: Array<Loan> = []
    masterSalary: MasterSalary
    investmentPlanComponents: Array<InvestmentPlanComponent> //array of components
    exemptions: any
    otherIncome: OtherIncome
    pfApplicable: boolean
    salaryChanged: boolean
    esicApplicable: boolean
    nonCtcComponents: any
    perks: Array<Perk> = []
    total_fbp_declared: number
    rent: number // you will get this and metro both from employee_rental table
    metro: boolean

    //specialAllowance        : number    //will come from FBP engine after the fbp process have been done

    // constructor(obj:any , legalEntity){
    //     this.attendance = new Attendance(obj.attendance)
    //     this.location = legalEntity.Locations(obj.location.id)
    //     this.ownerLegalEntity = legalEntity
    //     this.inputComponents = obj.inputComponents
    //     this.tdsDeducted = obj.tdsDeducted
    //     this.loans = obj.loans
    // }
    constructor(obj: any, legalEntity: LegalEntity, month: string) {

        this.employee_id = obj.employee_id
        this.attendance = obj.EmployeeAttendance
        this.location = legalEntity.locations[obj.based_location_id]
        // {"id":obj.based_location_id}
        // legalEntity.locations[obj.based_location_id]
        this.ownerLegalEntity = legalEntity
        this.inputComponents = obj.inputComponents // todo : hemant build in handler change t as per new design
        // this.inputComponents = SalaryDescription.getComponentList(obj.inputComponents)
        this.tdsDeducted = obj.tdsDeducted
        this.tdsOverride = obj.tdsOverride
        this.loans = obj.EmployeeLoans
        this.salaryStructure = obj.salaryStructure
        this.salary = new Salary()
        this.salaryRegister = obj.EmployeeSalaryRegisters //- empty []
        this.masterSalary = new MasterSalary()

        // this.salaryRegister.masterSalaryList[month] = this.master_salary_structure
        this.investmentPlanComponents = obj.EmployeeInvestmentPlanComponent
        this.section10Exemptions = obj.section10Exemptions

    }

    public getAttendanceRatio(baseDays: number) {
        // console.log('attanedance ratio',baseDays)
        if (baseDays != 0) {
            return (baseDays - this.attendance.getLopDays() + this.attendance.getRLopDays()) / baseDays
        }
        return 0
    }

    public getPrevMonthMasterSalary(month: Date): MasterSalary {
        let prevKey = this.getPrevMonthKey(month)
        return this.salaryRegister.masterSalaryList[prevKey]
    }

    public getPrevMonthSalary(month: Date): number {
        let prevKey = this.getPrevMonthKey(month)
        if (this.salaryRegister.masterSalaryList[prevKey]) { //return will always be true
            let lastSalary = this.salaryRegister.masterSalaryList[prevKey]
            if (lastSalary != null && lastSalary.netSalary != null) {
                return lastSalary.netSalary
            }
        }
        return 0
    }

    //This is order N we will improve it we get sorted list we can return as soon as we find 1st match
    public getSalaryStructure(date: Date): EmployeeSalary {
        let inputDate = date
        let found = false
        let structure = null
        if (this.salaryStructures.length > 0) {
            this.salaryStructures.forEach(element => {
                if (element.dateApplicable > date && element.dateUpdated > date) {
                    if (found) {
                        if (element.dateUpdated > inputDate) {
                            structure = element
                            inputDate = element.dateUpdated
                        }
                    }
                    else {
                        found = true
                        inputDate = element.dateUpdated
                        structure = element
                    }
                }
            });
        }
        return structure
    }

    public checkSalaryChangedForArrears(date: Date) {
        if (this.salaryStructures != null) {
            this.salaryStructures.forEach(element => {
                element.dateUpdated = new Date()
                element.dateApplicable = new Date()
                if (element.dateUpdated.getFullYear() == date.getFullYear() && element.dateUpdated.getMonth() == date.getMonth()) {
                    if (element.dateApplicable < date) {
                        return element
                    }
                    return null
                }
            });
        }
        return null
    }

    public getNumberOfWorkingMonths() {
        return 12 // place holder update with proper function
    }

    public getMasterSalaryMonth(month: Date): MasterSalary {
        return this.salaryRegister.masterSalaryList[this.getKey(month)]
    }

    public getInvestmentPlanComponent() {
        var key: any;
        let investment = []
        //console.log(this.investmentPlanComponent)
        for (var i in this.investmentPlanComponent) {
            //console.log(this.investmentPlanComponent[key])
            let obj = {} as any
            /*if (this.investmentPlanComponent[key]["planned"]){
               //console.log(this.investmentPlanComponent[key].investmentComponentType.name)
               obj.investmentComponentType = { abbreviation :this.investmentPlanComponent[key].investmentComponentType.abbreviation,
                name : this.investmentPlanComponent[key].investmentComponentType.name,
               maximumAmount :this.investmentPlanComponent[key].investmentComponentType.maximumAmount
           }
               obj.planned = this.investmentPlanComponent[key].planned
               obj.actual = this.investmentPlanComponent[key].actual
               obj.audited = this.investmentPlanComponent[key].audited
               obj.approved = this.investmentPlanComponent[key].approved
           // }
           
        }*/
            investment.push(obj)

        }
        return investment
    }


    public SeniorCitizen(): boolean {
        return (this.age >= C.SeniorCitizenAge)
    }

    public getRent(month: string) {
        return 100
    }

    public getKey(date: Date): string {
        return (date.getFullYear().toString().substr(-2) + (date.getMonth() + 1).toString())
    }


    getPrevMonthKey(date: Date): string {
        let prevMonthDate = new Date(date)
        prevMonthDate.setMonth(prevMonthDate.getMonth() - 1)
        return this.getKey(prevMonthDate)
    }

    getPrevKeyFromKey(key: string) {

    }
}

export function getAttendanceRatio(options) {
    return this.getAttendanceRatio(options);
}


