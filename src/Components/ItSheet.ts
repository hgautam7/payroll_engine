import { HRA } from "./HRA";
import { SalaryDescription } from "./SalaryDescription";
import { ItSheetDescription } from "./ItSheetDescription";
import { SalaryComponent } from "./SalaryComponent";
import * as S from "../statuary/StatutoryConfig"
import * as C from "../utils/Config"
import { TdsOverride, TdsOverrideType } from "./TdsOverride";
//import { VariableTotal } from "src/utils/Config";


export class ItSheet {
    tdsOveride: TdsOverride
    totalTax: number
    incomeTax: number
    surcharge: number
    cess: number        
    inputTds: number    
    marginalRelief: number
    tds : number
    totalTaxProjected: number
    tdsPaidTillLast: number
    tdsPerMonthProjected: number

    hraValue: {previous:HRA , current : HRA , projected : HRA , exemption :number}
    // keys present in database and needed for payslip generation
    itEarningDescription: { [key: string]: ItSheetDescription }
    totalEarning: number
    
    
    sectionTenExemptions: { [key: string]: { name: string, exemption: number, paid: number } }
    totalSectionTenExemption: number
    
    sectionSixteenDeductions: [{ name: string, value: number }]
    totalSectionSixteenDeduction: number
    
    sectionSixAValues: { [key: string]: { total: number, allowed: number , Description : Array<{name:string , value : number}> } }
    totalSectionSixADeduction: number
    
    section24Deduction: { LossOnHouseProperty: number, InterestOnHousingLoan: number, TotalDeduction: number }
    otherIncome: [{ name: string, value: number }]
    totalOtherIncome : number
    totalSection24Deduction : number

    perquisites: { total: number, superannuationPerk: number }
    totalPerkValue : number

    totalPfPaidTillNow : number
    totalVpfPaidTillNow : number
    
    hraEarning: number
    taxableIncome: number

    constructor() {

    }

    addThisMonthIncome(salDescription: SalaryComponent, value) {
        if (salDescription.isTaxable()) {
            if (this.itEarningDescription[salDescription.code] == null) {
                this.itEarningDescription[salDescription.code] = new ItSheetDescription(salDescription)
            }
            this.itEarningDescription[salDescription.code].currentMonth += value
            if (salDescription.itSection == S.SectionTen) {
                this.sectionTenExemptions[salDescription.code]
                this.sectionTenExemptions[salDescription.code].paid += value
            }
        }
    }

    addPrevTaxableIncome(salDescription: SalaryComponent, value) {

        if (salDescription.isTaxable()) {
            if (this.itEarningDescription[salDescription.code] == null) {
                this.itEarningDescription[salDescription.code] = new ItSheetDescription(salDescription)
            }
            this.itEarningDescription[salDescription.code].prevMonth += value
            if (salDescription.itSection == S.SectionTen) {
                this.sectionTenExemptions[salDescription.code].paid += value
            }
        }
    }

    addProjectedTaxableIncome(salDescription: SalaryComponent, value) {

        if (salDescription.isTaxable()) {
            if (this.itEarningDescription[salDescription.code] == null) {
                this.itEarningDescription[salDescription.code] = new ItSheetDescription(salDescription)
            }
            this.itEarningDescription[salDescription.code].projected += value
            if (salDescription.itSection == S.SectionTen) {
                this.sectionTenExemptions[salDescription.code].paid += value
            }
        }
    }

    getMonthEarning(code : string){
        if (this.itEarningDescription[code] != null) {
            return this.itEarningDescription[code].currentMonth
        }
        return 0
    }

    getProjectedEarning(code : string) {
        if (this.itEarningDescription[code] != null) {
            return this.itEarningDescription[code].projected
        }
        return 0
    }

    getPreviousEarning(code : string) {
        if (this.itEarningDescription[code] != null) {
            return this.itEarningDescription[code].prevMonth
        }
        return 0
    }

    getAmountTillDate(code:string) {
        if (this.itEarningDescription[code] != null) {
            return this.itEarningDescription[code].getAmountTillDate()
        }
        return 0
    }

    checkHraPaid(){
        if (this.sectionTenExemptions[C.HRA].paid == this.itEarningDescription[C.HRA].getTotal()){
            return true
        }
        console.log(this.sectionTenExemptions[C.HRA].paid , this.itEarningDescription[C.HRA].getTotal())
        return (Math.abs(this.sectionTenExemptions[C.HRA].paid - this.itEarningDescription[C.HRA].getTotal()) <= 5)
    }

    calculateTaxableIncome() : number  {
        let totalIncome = this.totalEarning + this.totalPerkValue + this.totalOtherIncome  
        let totalDeduction =  this.totalSectionSixADeduction + this.totalSectionSixteenDeduction + this.totalSectionTenExemption + this.totalSection24Deduction
        this.taxableIncome = totalIncome - (isNaN(totalDeduction) ? 0 : totalDeduction)
        return this.taxableIncome
    }
    getTaxableIncome(){
        this.taxableIncome
    }

    calculateTds(monthRemaining : number){
        if (this.tdsOveride != null){
            switch (this.tdsOveride.type) {
                case TdsOverrideType.fixed: 
                    this.tds = this.tdsOveride.value
                case TdsOverrideType.last_month:
                    this.tds = this.tdsOveride.value
                case TdsOverrideType.zero:
                    this.tds = 0
                default : 
                    this.tds = this.getTds(monthRemaining)
            }
        } else {
            this.tds = this.getTds(monthRemaining)
        }
        this.totalTaxProjected = (isNaN(this.totalTax) ? 0 : this.totalTax) - (isNaN(this.tdsPaidTillLast) ? 0 : this.tdsPaidTillLast) - ((this.inputTds == undefined) ? 0 : this.inputTds)
        if (monthRemaining > 0) {
            this.tdsPerMonthProjected = this.totalTaxProjected / monthRemaining
        }        
    }

    getTds(monthRemaining){
        let taxRemaning = (isNaN(this.totalTax) ? 0 : this.totalTax) - (isNaN(this.tdsPaidTillLast) ? 0 : this.tdsPaidTillLast) - ((this.inputTds == undefined) ? 0 : this.inputTds)
        return Math.round (taxRemaning / (monthRemaining + 1))
    }

    addSectionTenExemptionPaid(salComp : SalaryComponent , value : number){
        if (salComp.itSection == S.SectionTen) {
            if (this.sectionTenExemptions[salComp.code] == null) {
                this.sectionTenExemptions[salComp.code] = {name:salComp.name , exemption:0 , paid:0}}
            }
            this.sectionTenExemptions[salComp.code].paid += value
        }
        //sectionTenExemptions: { [key: string]: { name: string, declare: any, exemption: number, paid: number } }
    
    assignSectionTenExemption(key : string , value : number){
        if (this.sectionTenExemptions[key] == null){
            throw new Error("Wrong Exemption")
        }        
        this.sectionTenExemptions[key].exemption += value
        this.totalSectionTenExemption += value     
    }    

    setPerquisites(total : number , superannuation_perk : number){
        this.perquisites.total = total
        this.perquisites.superannuationPerk = superannuation_perk
    }
    
    addSectionSixteenDeduction(name : string , value : number){ 
        if(this.sectionSixteenDeductions){
            this.sectionSixteenDeductions.push({name: name , value : value})
            this.totalSectionSixteenDeduction += value
            //[{ name: string, value: number }]
        }
     }
 
    addSection24Deduction() {
        // LossOnHouseProperty: number, InterestOnHousingLoan: number, TotalDeduction: number
     }
    addSectionSixAValues(key : string , name : string , value : number){
    if(this.sectionSixAValues){
        if (this.sectionSixAValues[key] == null){
            this.sectionSixAValues[key].Description = new Array()
            this.sectionSixAValues[key].total = value
        } else {
            this.sectionSixAValues[key].total += value
        }
        this.sectionSixAValues[key].Description.push({name:name , value:value})
      } 
    }                
     
    addOtherIncome(){
     //[{ name: string, value: number }]
    }

    getLastSheetData(lastSheet : ItSheet){        
        if (lastSheet == null )
            return 
        let hraObj = new HRA()
        hraObj.allowedBasic = lastSheet.hraValue.previous.allowedBasic + lastSheet.hraValue.current.allowedBasic
        hraObj.exemption = lastSheet.hraValue.previous.exemption + lastSheet.hraValue.current.exemption
        hraObj.receieved = lastSheet.hraValue.previous.receieved + lastSheet.hraValue.current.receieved
        hraObj.rentMinusBasic = lastSheet.hraValue.previous.rentMinusBasic + lastSheet.hraValue.current.rentMinusBasic
        this.hraValue.previous = hraObj
        this.totalPfPaidTillNow = lastSheet.totalPfPaidTillNow
        this.totalVpfPaidTillNow = lastSheet.totalVpfPaidTillNow
        this.perquisites.total = lastSheet.perquisites.total
        this.perquisites.superannuationPerk = lastSheet.perquisites.superannuationPerk
        this.tdsPaidTillLast = lastSheet.tdsPaidTillLast + lastSheet.tds + lastSheet.inputTds 
        if (this.tdsOveride != null && this.tdsOveride.type == TdsOverrideType.last_month) {
            this.tdsOveride.value = lastSheet.tds
        }
    }
}