export class EmployeeESIC {

    applicabilityWage : number
    wage:number
    eligibility:boolean 
    employeeContribution:number
    employerContribution:number
    checkNext: boolean
    applicable: boolean

    constructor(){
        this.wage = 0
        this.employeeContribution = 0
        this.employerContribution = 0
        this.applicabilityWage = 0
    }
}