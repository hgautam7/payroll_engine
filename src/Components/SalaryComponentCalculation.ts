/***
 * @author           : Saurabh
 * @Creation_Date    : 16th Oct 2019
 * @Description      : Master Salary Component 
 * 
 * @Modification_Date : 21st Oct 2019 
 * @Modified_By       : Hemant 
 * @Description       : Initialized instance varibles in constructor 
 */


export class SalaryComponentCalculation {
    effectiveDate : Date
    formula : any
    unit : string

    constructor(obj : any){
        this.effectiveDate = new Date("01/jan/2019") //obj.effective_date, for time being till priyanka fixes the bug
        this.formula = obj.formula,
        this.unit = obj.unit
    }
    
}