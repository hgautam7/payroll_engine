export class TdsOverride {
    id 
    employee_id 
    month : string
    type : TdsOverrideType
    value : number
    year : string

    constructor(obj :any){
        this.id = obj.id
        this.employee_id = obj.employee_id
        this.month = obj.month
        this.type = obj.tds_override
        this.value = obj.tds_override_value
        this.year = obj.year
    }
}


export enum TdsOverrideType {
    fixed,
    last_month,
    zero,
    no_override
}