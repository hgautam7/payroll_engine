import { LOP } from "./LOP"

export class Attendance {
    
    baseDays : number
    lop : Array<LOP>
    rLop : Array<LOP>


    constructor(obj : any){
        this.lop = obj.lop
        this.rLop = obj.rLop
    }

    getLopDays(){
        return this.lop.length
    }

    getRLopDays(){
        return this.rLop.length
    }

    
}