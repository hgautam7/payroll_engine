import * as C from "../utils/Config"

export class Loan {
    
    amount : number
    repaymentStartDate:Date
	type:string    
    salary_component_id:""
	recoveryType: string //Monthly,Quaterly,Half Yearly/Yearly
    principle : number
    interest : number
    openingBalance : number
    roundOffValue : number
    schedule : {date:Date , emi:number , principle:number}
    totalPaid : number
    paidInInterest : number
    installments : {[key:string] : Installment}



    constructor(obj){
        //write
        this.amount = obj.amount
        this.installments = obj.installments
    }

    getInstallment(month){
        return this.installments[month]        
    }
}

export class Installment {
    date:Date
    amount:number
    month:string
    interest:number
    cb:number
    status:string
}