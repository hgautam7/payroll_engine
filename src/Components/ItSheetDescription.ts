export class ItSheetDescription {
    name : string;
    code : string;
    prevMonth : number;
    currentMonth : number;
    projected : number;

    constructor(obj : any) {
        this.name = obj.name
        this.code = obj.code
        this.prevMonth = 0
        this.currentMonth = 0
        this. projected = 0
    }

    getAmountTillDate ():number{
        return this.prevMonth + this.currentMonth
    }

    getTotal() :number {
        return this.prevMonth + this.currentMonth + this.projected
    }
}
