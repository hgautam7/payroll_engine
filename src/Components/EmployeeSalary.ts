/***
 * @author           : Saurabh
 * @Creation_Date    : 16th Oct 2019
 * @Description      : Master Salary Component 
 * 
 * @Modification_Date : 21st Oct 2019 
 * @Modified_By       : Hemant 
 * @Description       : Initialized salaryStructureId in constructor 
 * 
 *  
 * @Modification_Date : 24th Oct 2019 
 * @Modified_By       : Hemant 
 * @Description       : Changed how we compare Enum.value with this.calculation to Enum[value] in SSSalaryComponentValue constructor
 */

import { Util } from '../utils/util';
import { SalaryComponent, SalaryComponentCalculationType } from "./SalaryComponent"
import { SalaryDescription } from "./SalaryDescription";
//import { type } from "os";
import * as C from "../utils/Config"
import { LegalEntity } from './LegalEntity';
import { CustomConsole } from '@jest/console';

// import * as InputData from "../TestData/InputData";
// console.log(InputData)
export class EmployeeSalary {

    custom: boolean
    salaryStructureId: string
    public ctc: number;
    variablePay: number;
    public salaryComponents: Array<SalaryDescription>;
    specialAllowance: number //is there in structure
    dateUpdated: Date
    dateApplicable: Date
    from: Date
    to: Date
    exemptForITFirstMonth: boolean
    // vpf = {} as {type: string , value: number} = {type : C.VoluntaryPF, value}
    vpf: { type: string, value: number } = { type: C.VpfFixed, value: 5000 }

    constructor(obj: any) {
        this.salaryComponents = obj.salaryComponents
        this.salaryStructureId = obj.salaryStructureId
        this.ctc = obj.ctc
    }

    // if it is non custom calculate Salary Structure on the fly
    generateSalaryComponents(legalEntity: LegalEntity, date: Date) {
       // console.log("******* Inside generateSalaryComponents ***********")
        this.salaryComponents = []
        let formula 
        var salComps = {} as { [key: string]: SSSalaryComponentValue }
        console.log("this.salaryStructureId = ", this.salaryStructureId)
        console.log("LE structure  = ", legalEntity.salaryStructures)
        legalEntity.salaryStructures[this.salaryStructureId].salaryComponents.forEach((salaryComponentID: any) => {
           // console.log("Calculation Array = ", legalEntity.salaryComponentsID[salaryComponentID].calculation)
            let salComp = legalEntity.salaryComponentsID[salaryComponentID]
            //console.log("component name = ",salComp.name)
            formula = salComp.getFormula(date)
            if(salComp.type == "FLAT" && formula == undefined){
              formula = 0 // this is temp workaround , as UI is sending wrong .post demo correct it 
            }
            let obj = { code: salComp.abbreviation, value: 0, calculationType: salComp.calculationType, calculationDone: false, formula: formula }
            let ssSalComp = new SSSalaryComponentValue(obj)
        });
        var tempObj = { code: C.CTC, value: this.ctc, calculationType: SalaryComponentCalculationType[SalaryComponentCalculationType.FLAT], calculationDone: true, formula: this.ctc }
        let ctcSalComp = new SSSalaryComponentValue(tempObj)
        salComps[C.CTC] = ctcSalComp;
        let calVal = 0
        for (let key in salComps) {
            if (salComps[key].code == C.SpecialAllowance || salComps[key].code == C.CTC) {
                continue
            }
            var ssSalComp = salComps[key]
            this.calculateSSSalaryComponent(ssSalComp)
            if (ssSalComp.code == C.VariableTotal) {
                this.variablePay = ssSalComp.value
            }
            else {
                let salDesc = new SalaryDescription(ssSalComp, ssSalComp.value)
                this.salaryComponents.push()
            }

            calVal += ssSalComp.value
        }
        this.specialAllowance = this.ctc - calVal
        if (this.specialAllowance < 0) {
            throw new Error("Wrong Salary Structure")
        }
    }

    calculateSSSalaryComponent(salaryComponent: any) {
        if (!salaryComponent || salaryComponent.calculationDone) {
            return
        }
        console.log("salaryComponent.getDependantComponents() = ", salaryComponent.getDependantComponents())
        var dependantComponentKeys = salaryComponent.getDependantComponents()
        if (dependantComponentKeys != null) {
            dependantComponentKeys.forEach((code: any) => {
                if (code == null || code.trim().length == 0) {
                } else {
                    this.calculateSSSalaryComponent(this.salaryComponents[code])
                }
            });
        }
        salaryComponent.calculate(this.salaryComponents)
    }




    getSalaryComponentValue(abbr: string) {
        for (const key of this.salaryComponents) {
            if (key.code == abbr) {
                return key.value
            }
        }
    }


    getSalaryComponentProjectedValue(abbr: string, months: number) {
        for (const key of this.salaryComponents) {
            if (key.code == abbr) {
                return (key.value * months)
            }
        }
    }
}

export class SSSalaryComponentValue {
    code: any
    calculationType: any
    calculationDone: boolean
    value: number
    formula: string

    constructor(obj: any) {
        this.code = obj.code
        this.calculationType = obj.calculationType
        if (this.calculationType == SalaryComponentCalculationType[SalaryComponentCalculationType.FLAT]) {
            this.value = obj.formula
            this.calculationDone = true
        } // Changed how we compare Enum.value to be compared with this.calculation to Enum[value] -- as this give string valueOf(Enum value)
        else if (this.calculationType == SalaryComponentCalculationType[SalaryComponentCalculationType.FORMULA]) {
            this.formula = obj.formula
            this.calculationDone = false
        }
        else {
            throw new Error("Wrong Calculation Type")
        }
    }

    getDependantComponents() {
        if (this.calculationType == SalaryComponentCalculationType.FLAT)
            return null
        var regex = /[A-Za-z]*/g
        console.log("Formula = ", this.formula)
        return this.formula.match(regex)
    }

    calculate(salaryComponents: any) {
        // console.log('here')
        if (this.calculationType == SalaryComponentCalculationType.FLAT)
            return;
        var FormulaParser = require('hot-formula-parser').Parser
        var parser = new FormulaParser()
        var dependantComponents = this.getDependantComponents()
        for (var index = 0; index < dependantComponents.length; index++) {
            var element = dependantComponents[index];
            if (element == null || element.trim().length == 0) {
                // console.log('here again')    
            } else {
                parser.setVariable(element, salaryComponents[element].value)
            }
        }
        var abc = this.formula.replace("%", "/100*")
        console.log("formula => ", abc)
        this.value = parser.parse(this.formula).result
        // console.log(1)          
        // }
        this.calculationDone = true
    }
}

let a = {
    "ctc": 200000,
    "salaryComponents": [{
        "name": "Basic",
        "calculationType": "formula",
        "abbreviation": "Basic",
        "formula": "CTC/2",
        "monthly": 0,
        "annual": 0
    }, {
        "name": "pf",
        "abbreviation": "PF",
        "calculationType": "formula",
        "formula": "(Basic*12%)-100+(200)",
        "monthly": 12000,
        "annual": 240000
    }, {
        "name": "esi",
        "calculationType": "formula",
        "abbreviation": "ESI",
        "formula": "PF*10",
        "monthly": 12000,
        "annual": 240000
    }, {
        "name": "hra",
        "formula": "Basic*12%",
        "calculationType": "formula",
        "abbreviation": "HRA",
        "monthly": 0,
        "annual": 0
    }, {
        "name": "conveyance",
        "calculationType": "fixed",
        "abbreviation": "CON",
        "formula": 0,
        "value": 240000,
        "monthly": 12000,
        "annual": 240000
    }, {
        "name": "medicalAllowance",
        "calculationType": "formula",
        "abbreviation": "MA",
        "formula": "Basic*12%",
        "monthly": 12000,
        "annual": 240000
    }, {
        "name": "lta",
        "formula": "Basic*12%",
        "calculationType": "formula",
        "abbreviation": "LTA",
        "monthly": 0,
        "annual": 0
    }, {
        "name": "specialAllowance",
        "formula": "Basic*12%",
        "calculationType": "formula",
        "abbreviation": "SA",
        "monthly": 0,
        "annual": 0
    }]
}
