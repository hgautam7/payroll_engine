import { SalaryDescription } from "./SalaryDescription";
import { Util } from "../../src/utils/util";

export class NonCtCComponent extends SalaryDescription {


    financialYear: string
    assignedDate: Date
    value: number
    from: Date
    to: Date
    

    checkForArrears(date: Date): boolean {
        if (this.assignedDate.getMonth() == date.getMonth() && this.assignedDate.getFullYear() == date.getFullYear()) {
            if (this.from.getFullYear() < date.getFullYear()) {
                return true
            }
            else if (this.from.getMonth() < date.getMonth()) {
                return true
            }
        }
        return false
    }

    getArrearValue(date : Date){
        if (date >= this.from && date < this.assignedDate){
            let value = this.getMonthValue(date)
        }
        return 0
    }

    getValue(date: Date) {
        if (this.checkApplicable(date)) {
            return this.getMonthValue(date)
        }
        return 0
    }

    private getMonthValue(date : Date){
        if (date.getMonth() == new Date(this.from).getMonth() && date.getFullYear() == new Date(this.from).getFullYear()) {
            let daysInMonth = Util.getDaysInAMonth(new Date(this.from).getFullYear(), new Date(this.from).getMonth())
            let totalDays = daysInMonth - new Date(this.from).getDate() + 1
            return this.value * totalDays / daysInMonth
        }
        else if (this.to != null && date.getMonth() == new Date(this.to).getMonth() && date.getFullYear() == new Date(this.to).getFullYear()) {
            let daysInMonth = Util.getDaysInAMonth(new Date(this.from).getFullYear(), new Date(this.from).getMonth())
            let totalDays = new Date(this.to).getDate()
            return this.value * totalDays / daysInMonth
        }
        else {
            return this.value
        }
    
    }

    public getSalDescription(value) : SalaryDescription{
        return new SalaryDescription(this , value)
      }

    public checkApplicable(date) {
        if (date >= this.from && date > this.assignedDate) {
            if (this.to == null || this.to >= date) {
                return true
            }
        }
        return true

    }
}