/**This component is for single  */

import { Employee } from "./Employee"

export class Arrear {
    id: string
    employeeId: string
    financialYear: string
    payrollMonth: string
    forMonth: number
    salaryComponentId: string
    code: string
    name: string
    value: number
    status: boolean
    paidOn: Array<{ date: Date, amount: number }>
    //@added by hemant
    salaryCompMap: [{ key: string }, Object] // key = yymm and value SalaryComp
    processingKey: string

    public constructor(obj: Employee) {
        this.employeeId = obj.employee_id
        this.financialYear = obj.financialYear
        this.payrollMonth = obj.processingMonth
        this.forMonth = new Date(obj.salaryStructure.from).getMonth();//not sure of fromMonth -- it will come from UI as effective date ? 
        this.salaryComponentId = obj.salaryComponentId
        this.code = obj.code
        this.name = obj.name
        this.value = obj.value
        this.status = obj.status
        this.paidOn = obj.paidOn
        this.salaryCompMap = obj.salaryCompMap
        this.processingKey = this.financialYear.substring(3, 4) + (this.getMonth(this.payrollMonth))
    }

    getMonth(month: string) {
        return new Date(month + '-1-01').getMonth() + 1
    }

//move this functions in employee handler
    getSalCompMapArr() : Array<any>{
        let salCompMapArr : any []
        salCompMapArr.push(this.salaryCompMap[this.processingKey])
        return salCompMapArr
    }

}