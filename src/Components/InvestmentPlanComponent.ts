import { InvestmentComponentType } from "./InvestmentComponentType";

export class InvestmentPlanComponent{
    investmentComponentTypeID : string // this will be id main list will be in Engine 
    planned : number
    actual : number
    approved : number
    audited : boolean
   // remarks : varchar
    //audit_remarks varchar
    //file_locations varchar[]
    //status investment_proof_status
    //created date
    //updated date

    constructor(obj){
        this.investmentComponentTypeID = obj.investmentComponentType
        this.planned = obj.planned
        this.actual = obj.actual
        this.approved = obj.approved
        this.audited = obj.audited
    }
}