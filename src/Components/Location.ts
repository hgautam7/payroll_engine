import { AddressObj } from "./AddressObj";

export enum Deduction_Frequency {
    monthly,
    annually,
    quaterly,
    bi_annually 
}

export class Location {
    
    id; 
    name : string
    address : AddressObj
    
    
    ptApplicable : boolean
    ptStateId : string
    ptDeductMonthly : boolean  
    
    lwfApplicable : boolean
    wwfApplicable : boolean
    lwfAddress  :  AddressObj
    
    // lwfStateId : string
    // lwfState : string
    
    
    esiDeductionFrequency : Deduction_Frequency
    esiApplicable : boolean
    standardEsiContribution : boolean

    constructor(obj : any){
        this.address = obj.address
        this.id = obj.id
        this.ptStateId = obj.ptStateId
        this.lwfApplicable = obj.lwfApplicable
        this.wwfApplicable = obj.wwfApplicable
        this.lwfAddress = obj.lwfAddress
        //this.deductionApplicability = obj.deductionApplicability
        this.esiApplicable = obj.esiApplicable
        this.standardEsiContribution = obj.standardEsiContribution
    }

    getLwfState(){
        return this.lwfAddress.state
    }
}
