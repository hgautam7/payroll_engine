/***
 * @author           : Saurabh
 * @Creation_Date    : 16th Oct 2019
 * @Description      : Master Salary Component 
 * 
 * @Modification_Date : 21st Oct 2019 
 * @Modified_By       : Hemant 
 * @Description       : Initialized id in constructor & added datatype [any] to various arguments or instance variables  
 */

import * as C from "../utils/Config"
import { Util } from "../../src/utils/util"
import { SalaryComponentCalculation } from "./SalaryComponentCalculation"

export enum SalaryComponentCalculationType {
    FLAT,
    FORMULA,
    SLAB
}

export class SalaryComponent {
    // code
    // category_code
    code: string
    abbreviation : string
    id: any 
    name : string
    mappingCategory: string
    paid: boolean
    taxable: boolean
    roundOff: string
    monthly: boolean
    annual: boolean
    type: string
    grossUpTax: boolean
    fixed: boolean
    payFrequncyId: string
    calculationType: SalaryComponentCalculationType
    calculation: Array<SalaryComponentCalculation>
    calculationOnActual: boolean
    earning: boolean
    notAttendanceDependant: boolean
    payableIfFullLop: boolean
    superannuationPerk: boolean
    minimumThreshold: number
    payableIfEmployeeHaveJoinedBeforeDate: boolean
    cutoffDate: number // }
    itSection: string
    itSubSection: string
    considerEsiDeduction: boolean
    considerEsiEligibility: boolean
    ptApplicability: boolean



    public constructor(obj: any) {
        this.code = obj.code
        this.name = obj.name
        // this.category_code = obj.category_code
        this.id = obj.id
        this.abbreviation = obj.abbreviation
        this.type = obj.calculation_type
        this.paid = obj.paid
        // this value is in salary desc here
        this.considerEsiDeduction = obj.consider_esi_deduction
    }

    getValue() { }

    calculate(value: any, ratio: any) {
        if (this.notAttendanceDependant) {
            return Util.getRoundOff(value / 12, this.roundOff)
        }
        return Util.getRoundOff(value / 12 * ratio, this.roundOff)
    }

    isTaxable(): boolean {
        return this.taxable
    }

    isPartOfPtWage(): boolean {
        return this.ptApplicability
    }

    isPartofESIEligibility(): boolean {
        return this.considerEsiEligibility
    }

    isPartOfESIDeduction(): boolean {
        return this.considerEsiDeduction
    }

    isEarning(): boolean {
        return this.paid
    }

    isDeduction(): boolean {
        if(this.type == "DEDUCTION"){
         return true
        } else {
         return false
        }
    }

    isPartOfGross(): boolean {
        return true
    }

    getPerkType(): string {
        return C.CLAPerk
        //get the info from mapping category 
    }

    calculatePerkValue(amount: number, custom: any): number {
        if (this.getPerkType() == C.CarPerk) {
            return this.calculateCarPerk(amount, custom)
        }
        else if (this.getPerkType() == C.CLAPerk) {
            return this.calculateCLAPerk(amount, custom)
        }
        else
            return amount
    }

    calculateCarPerk(amount: number, custom: any): number {
        return amount
    }

    calculateCLAPerk(amount: number, custom: any): number {
        return amount
    }

    getFormula(date: Date) {
        if (this.calculation.length > 0){
            let formula = this.calculation[0].formula
            let effectiveDate = new Date(1990,1,1)
            this.calculation.forEach(element => {
                if (date > element.effectiveDate && effectiveDate < element.effectiveDate) {
                    formula = element.formula
                    effectiveDate = element.effectiveDate
                }
            });
            return formula
        }
        return 0
       }
    }        