const { ServiceBroker } = require("moleculer");
export const broker = new ServiceBroker({ transporter: "NATS" });