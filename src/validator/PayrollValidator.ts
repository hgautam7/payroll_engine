/***
 * @author           : Hemant Joshi
 * @Creation_Date    : 27th Sept 2019
 * @Description      : Validates the Employee response for payroll computation 
 *                     Else returns backs errors to the screen. 
*/
const BaseValidator = require("moleculer").Validator
const {ValidationError} = require("moleculer").Errors
import { Employee } from "../../src/Components/Employee";

export class payrollValidator extends BaseValidator{
    employee_name : string 

  constructor(){
      super();
  }

  public validateEmployeeResponse(employees : Employee){
      employees.forEach((employee : Employee) => {
          if ( employee.EmployeeSalarySetting[0] == 'undefined' 
            || employee.EmployeeSalarySetting[0] == null){
                throw new ValidationError("Employee salary details are empty, please correct and then run Payroll !")
            }
      });

  }



}
